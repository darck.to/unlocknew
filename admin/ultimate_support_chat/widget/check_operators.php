<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With");

date_default_timezone_set('UTC');

include("../data/config/config.php");
$PDO = PDO_CONNECT();

$jsGlobalJson = file_get_contents('../data/config/presets/config.json');
$jsGlobalConfig = json_decode($jsGlobalJson, true);


if(isset($_POST['selectedPreset']) && is_numeric($_POST['selectedPreset']) && $_POST['selectedPreset'] !=''){
	$jsGlobalConfig['selectedPreset'] = trim($_POST['selectedPreset']);
}

$jsThemeJson = file_get_contents('../data/config/presets/preset_'.$jsGlobalConfig['selectedPreset'].'.json');
$jsThemeConfig = json_decode($jsThemeJson, true);

$time = time()-30;
$command_admin_ready = "SELECT * FROM users WHERE status > $time";
$result_admin_ready = $PDO->prepare($command_admin_ready);
$result_admin_ready->execute();
$num_rows = $result_admin_ready->rowCount();

$return = array('jsGlobalConfig' => $jsGlobalConfig , 'jsThemeConfig' => $jsThemeConfig, 'online_operators' => $num_rows);

echo json_encode($return);

die();