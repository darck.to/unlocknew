<?php

// GET APP DOMAIN & DIRECTORY //

function siteURL()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'] . '/';
    return $protocol . $domainName;
}

define('SITE_URL', siteURL());

$current_directory = $_SERVER['PHP_SELF'];
$curArr = explode('/', $current_directory);
unset($curArr[count($curArr) - 1]);
unset($curArr[count($curArr) - 1]);
$appDir = implode('/', $curArr);
$appPath = $_SERVER['DOCUMENT_ROOT'].$appDir;

$isInstalled = false;
if (file_exists($appPath . '/data/config/config.php')) {
    $isInstalled = true;
    header('Location: ' . siteURL() . $appDir . '/index.php');
    die();
}

if (isset($_POST['install']) && !$isInstalled) {
    $error = '';
	
} else {
    $_POST['mysql_host'] = '';
    $_POST['mysql_user'] = '';
    $_POST['mysql_pass'] = '';
    $_POST['mysql_db'] = '';
    $_POST['admin_username'] = '';
    $_POST['admin_password'] = '';
    $_POST['admin_mail'] = '';
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ultimate Support Chat - Installation</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo SITE_URL . $appDir . '/data/css/bootstrap.min.css'; ?>" type="text/css">
    <link rel="stylesheet" href="<?php echo SITE_URL . $appDir . '/install/css/install.min.css'; ?>" type="text/css">
    <!-- jQuery 2.1.4 -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

</head>
<body>

<header>
    <div class="header-content">
        <div class="header-content-inner">
            <h1>WELCOME TO ULTIMATE SUPPORT CHAT</h1>
            <hr>
            <p>To install the software you'll need to setup a database connection. Please review the installation
                instructions before continuing.</p>
            <a onclick="startInstall()" class="btn btn-primary btn-xl">Install</a>
        </div>
    </div>
</header>


<script>

    (function ($) {

        $.fn.fitText = function (kompressor, options) {

            var compressor = kompressor || 1,
                settings = $.extend({
                    'minFontSize': Number.NEGATIVE_INFINITY,
                    'maxFontSize': Number.POSITIVE_INFINITY
                }, options);

            return this.each(function () {

                var $this = $(this);

                var resizer = function () {
                    $this.css('font-size', Math.max(Math.min($this.width() / (compressor * 10), parseFloat(settings.maxFontSize)), parseFloat(settings.minFontSize)));
                };

                resizer();
                $(window).on('resize.fittext orientationchange.fittext', resizer);
            });

        };

    })(jQuery);

    $("h1").fitText(
        1.2, {
            minFontSize: '35px',
            maxFontSize: '65px'
        }
    );


    function startInstall() {
        $(".header-content-inner").html('');
        $('.header-content').css({'top': '30px', '-webkit-transform': 'initial', 'transform': 'initial'});
        $('#content').contents().appendTo('.header-content-inner');
    }


</script>

<div id="content">

    <?php
    if ($isInstalled) {
        ?>

        <div class="box_error">
            <strong>Your system has been installed!</strong>
            <br/><br/>
            <strong>Please DELETE installation folder /install/</strong>
        </div>
        <?php
    } elseif (isset($complete)) {
        ?>
        <div class="box_complete">
            <strong>Installation completed!</strong>
            <br/><br/>
            <a href="<?php echo '/' . $appDir . '/users/login' ?>" title="">Go to App Login page</a>
        </div>
        <?php
    }
    else {
    ?>

    <?php
    if (isset($error) && $error != '') {
        ?>
        <div class="box_error"><?php echo $error; ?></div>

    <?php }

    $fas_error = false;

    if (@substr(sprintf('%o', fileperms($appPath . '/engine/data/')), -4) != '0777') {
        $errorArr[] = '<div class="box_error"><strong>Required: </strong>Please set writing permission (0777) o the following folder: <strong>/engine/data</strong>.<br/>If you\'re using a Windows server set the folder as readable and writable.</div><br/>';
        $fas_error = true;
    }
    if (@substr(sprintf('%o', fileperms($appPath . '/data/config/')), -4) != '0777') {
        $errorArr[] = '<div class="box_error"><strong>Required: </strong>Please set writing permission (0777) on the following folder: <strong>/data/config</strong>.<br/>If you\'re using a Windows server set the folder as readable and writable.</div><br/>';
        $fas_error = true;
    }

    if (!function_exists('fopen')) {

        $errorArr[] = '<div class="box_error"><strong>Required: </strong>Function "fopen" should be active</div><br/>';
        $fas_error = true;
    }
    if (!function_exists('scandir')) {
        $errorArr[] = '<div class="box_error"><strong>Required: </strong>Function "scandir" should be active</div><br/>';
        $fas_error = true;
    }
    if (!version_compare("5.2", phpversion(), "<=")) {
        $errorArr[] = '<div class="box_error"><strong>Required: </strong>PHP >= 5.2 is required</div><br/>';
        $fas_error = true;
    }
	
	if (isset($_GET['mode']) && $_GET['mode'] == 0){
		$fas_error = false;
	}
	
    if ($fas_error == true) {

        ?>

        <h1>ULTIMATE SUPPORT CHAT INSTALLATION</h1>
        <br/>

        <div class="box box-info">
            <h3>PERMISSIONS CHECK</h3>
            <br/><br/>
        </div>

        <?php

        foreach ($errorArr as $e) {
            print_r($e);
            echo '<br/>';
        }
		
		$alert = "Are you sure?";
		echo '<small><a style="color:#000000" href="'.siteURL() . $appDir . '/install/install.php?mode=0" onclick=\'alert("Are you sure? Please make sure the requested folders are readable and writable")\'>Ignore error and continue Installation</a></small>';
		
    }


    if (!$fas_error) {

    ?>
    <div>
        <div id="db_connection">
            <h1>ULTIMATE SUPPORT CHAT INSTALLATION</h1>
            <br/>

            <div class="box box-info">
                <br/>

                <h3>DATABASE SETUP</h3>

                <div class="sk-cube-grid">
                    <div class="sk-cube sk-cube1"></div>
                    <div class="sk-cube sk-cube2"></div>
                    <div class="sk-cube sk-cube3"></div>
                    <div class="sk-cube sk-cube4"></div>
                    <div class="sk-cube sk-cube5"></div>
                    <div class="sk-cube sk-cube6"></div>
                    <div class="sk-cube sk-cube7"></div>
                    <div class="sk-cube sk-cube8"></div>
                    <div class="sk-cube sk-cube9"></div>
                </div>


                <div id="db_status" class="db_status_red">&nbsp;</div>

                <div class="box-body">
                    <!-- form start -->
                    <form class="form-horizontal" action="" method="post" id="database_connection_form"
                          autocomplete="off">

                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" placeholder="Database host" id="db_host"
                                       name="db_host" value="localhost" required/>
                                <i class="pull-left">This would be localhost for most cases.</i>
                            </div>
                            <div class="col-sm-3"></div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" placeholder="Database name" id="db_name"
                                       name="db_name" value="" required/>
                                <i class="pull-left">Your database name</i>
                            </div>
                            <div class="col-sm-3"></div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <label class="input_null">
                                    <input class="input_null" type="text" name="null1"/>
                                    <input class="input_null" type="password" name="null2"/>
                                </label>
                                <input type="text" class="form-control" placeholder="User name" id="db_user"
                                       name="db_user" value="" autocomplete="off" required/>
                                <i class="pull-left">User name with access to the database</i>
                            </div>
                            <div class="col-sm-3"></div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" placeholder="Password" id="db_password"
                                       name="db_password" value="" autocomplete="off" required/>
                                <i class="pull-left">User password</i>
                            </div>
                            <div class="col-sm-3"></div>
                        </div>


                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary btn-xl">Test connection</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box-body -->
            </div>

            <script>

                if (document.getElementById("database_connection_form") != undefined) {
                    document.getElementById("database_connection_form").onsubmit = function () {
                        $('.sk-cube-grid').css('visibility', 'visible');

                        var dbHost = encodeURIComponent($('#db_host').val());
                        var dbName = encodeURIComponent($('#db_name').val());
                        var dbUser = encodeURIComponent($('#db_user').val());
                        var dbPassword = encodeURIComponent($('#db_password').val());

                        var xhttp = new XMLHttpRequest();
                        xhttp.onreadystatechange = function () {
                            if (xhttp.readyState == 4 && xhttp.status == 200) {
                                $('.sk-cube-grid').css('visibility', 'hidden');
                                if (xhttp.responseText == 'true') {
                                    $('#db_connection').hide();
                                    $('#setup_admin').show();

                                } else {
                                    $('#db_status').html('Database connection failed. Please check the credentials and try again.');
                                }
                            }
                        };
                        xhttp.open("POST", "process.php", true);
                        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                        xhttp.send("db_host=" + dbHost + "&db_name=" + dbName + "&db_user=" + dbUser + "&db_password=" + dbPassword + "&check_db=true");

                        return false;
                    }
                }

            </script>
        </div>


        <div id="setup_admin">

            <h1>ULTIMATE SUPPORT CHAT INSTALLATION</h1>
            <br/>

            <div class="box box-info">
                <br/>

                <div id="response" class="db_status_green">Database test sucessfull.</div>
                <h3>ADMIN ACCOUNT SETUP</h3>

                <div class="sk-cube-grid">
                    <div class="sk-cube sk-cube1"></div>
                    <div class="sk-cube sk-cube2"></div>
                    <div class="sk-cube sk-cube3"></div>
                    <div class="sk-cube sk-cube4"></div>
                    <div class="sk-cube sk-cube5"></div>
                    <div class="sk-cube sk-cube6"></div>
                    <div class="sk-cube sk-cube7"></div>
                    <div class="sk-cube sk-cube8"></div>
                    <div class="sk-cube sk-cube9"></div>
                </div>

                <div class="box-body">
                    <!-- form start -->
                    <form class="form-horizontal" action="" method="post" id="admin_setup_form" autocomplete="off">

                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <input type="email" class="form-control" placeholder="Email" id="admin_email"
                                       name="admin_email" value="" required/>
                                <i class="pull-left">Your email address</i>
                            </div>
                            <div class="col-sm-3"></div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <label class="input_null">
                                    <input class="input_null" type="text" name="null1"/>
                                    <input class="input_null" type="password" name="null2"/>
                                </label>
                                <input type="text" class="form-control" placeholder="User name" id="admin_name"
                                       name="admin_name" value="" autocomplete="off" required/>
                                <i class="pull-left">User name for application login</i>
                            </div>
                            <div class="col-sm-3"></div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" placeholder="Password" id="admin_password"
                                       name="admin_password" value="" autocomplete="off" required/>
                                <i class="pull-left">User password for application login</i>
                            </div>
                            <div class="col-sm-3"></div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary btn-xl">Complete</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box-body -->

            </div>

            <script>


                if (document.getElementById("admin_setup_form") != undefined) {
                    document.getElementById("admin_setup_form").onsubmit = function () {
                        $('.sk-cube-grid').css('visibility', 'visible');

                        var appPath = '<?php echo $appPath; ?>';
                        var adminEmail = encodeURIComponent($('#admin_email').val());
                        var adminName = encodeURIComponent($('#admin_name').val());
                        var adminPassword = encodeURIComponent($('#admin_password').val());
					
                        var xhttp = new XMLHttpRequest();
                        xhttp.onreadystatechange = function () {
                            if (xhttp.readyState == 4 && xhttp.status == 200) {
                                if (xhttp.responseText == 'installed') {
                                    $('#setup_admin').hide();
                                    $('#install_complete').show();
                                }
								else{
									$('#response').html(xhttp.responseText).css('color', '#FF0000');
								}
                            }
                        };
                        xhttp.open("POST", "process.php", true);
                        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                        xhttp.send("admin_email=" + adminEmail + "&admin_name=" + adminName + "&admin_password=" + adminPassword + "&app_path=" + appPath + "&admin_setup=true");

                        return false;
                    }
                }

            </script>
        </div>


        <div id="install_complete">
            <h1>ULTIMATE SUPPORT CHAT INSTALLATION</h1>
            <br/>

            <h3>Installation has completed successfully</h3>

            <p><a onclick="location.reload();" class="pointer">Go to Application Login page</a></p>

        </div>

        <?php } ?>

        <?php
        }
        ?>


    </div>
    <!-- end of content -->

</body>
</html>
