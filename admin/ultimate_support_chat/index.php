<?php

$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
$domain = $_SERVER['HTTP_HOST'];

$curArr = explode('/', $_SERVER['PHP_SELF']);
unset($curArr[count($curArr) - 1]);
$appDir = implode('/', $curArr);
$baseDir = ltrim($appDir, '/');

if(!file_exists(getcwd().'/data/config/config.php')) {
	header('Location: '.$protocol.$domain.'/'.$baseDir.'/install/install.php');
	die();
}
require_once 'config.php';
include_once $config ['engine_path'] . "/initEngine.php";
