//HOY
var today = new Date();

$(document).ready(function(){

	var editor = new MediumEditor('.editable', {
		toolbar: {
	        buttons: ['bold', 'italic', 'underline', 'h2', 'h4', 'quote']
	    }
	});

	//OBTENEMOS LOS DATOS DE JSON
	getcalendarData();

});

function getcalendarData(e) {

	$.ajax({
	    cache: 'false',
	    url: "php/requests/requests-getData.php",
	    context: document.body,
	    success: function(data) {

	    	//CALENDARIO
	      	$('#calendar').fullCalendar({
	      		nowIndicator: true,
	      		height: 650,
	      		//CLICKS
	      		eventClick: function(eventObj) {
					if (eventObj.code) {

						var code = eventObj.code;

						$.ajax({
						    type: "POST",
						    url: 'php/requests/requests-summary.php',
						    data: ({
						      	code: code
						    }),
						    dataType: "json",
						    success: function(result) {
						    	$('#calendarSummary').html('');
								$('#calendarSummary').append('<h6><small>Request Order:</small> <i class="blue-grey-text text-darken-3">' + result.random + '</i></h6>');
								$('#calendarSummary').append('<h6><small>Date:</small> <i>' + result.fecha + '</i><h6/>');
								$('#calendarSummary').append('<h6><small>IMEI:</small> <i class="blue-text">' + result.imei + '</i><h6/>');
								$('#calendarSummary').append('<h6><small>Customer Name:</small> <i class="blue-grey-text text-darken-3">' + result.usuario + '</i><h6/>');
								$('#calendarSummary').append('<h6><small>Email:</small> <i class="blue-grey-text text-darken-3">' + result.email + '</i><h6/>');
								$('#calendarSummary').append('<h6><small>Cost:</small> <i class="amber-text text-darken-4">$' + result.precio + '</i><h6/>');
								$('#calendarSummary').append('<h6><small>Comment:</small> <i>' + result.comentario + '</i><h6/>');
								if (result.estatus == 0) { var icon = 'thumb_down'; var color = 'red-text text-lighten-2';} else if (result.estatus == 1) { var icon = 'thumb_up'; var color = 'green-text text-darken-1';}
								$('#calendarSummary').append('<h6><small>Status:</small> <i class="blue-grey-text text-darken-3"><i class="material-icons ' + color + '">' + icon + '</i></i><h6/>');
								$('#calendarSummary').append('<h6><small>Code or Answer:</small> <i class="blue-grey-text text-darken-3">' + result.code + '</i><h6/>');
						    },
						    error: function(xhr, tst, err) {
						      console.log(err);
						    }
						});

					}
				},
				defaultView: 'month',
				displayEventTime: true,
				navLinks: true,
			    eventLimit: true,
				header: {
					left: 'month,agendaWeek,agendaDay',
					center: 'title',
					right: 'prev,next'
				},
				events: data
			})
	      
	    },
	    error: function(xhr, tst, err) {
	      console.log(err);
	    }
	})

};