// instanciate new modal
var modal = new tingle.modal({
    footer: true,
    stickyFooter: false,
    closeMethods: ['overlay', 'escape'],
    closeLabel: "Close",
    cssClass: ['custom-class-1', 'custom-class-2'],
    onOpen: function() {
    },
    onClose: function() {
    },
    beforeClose: function() {
        $('#unlockCode').remove();
		$('.sendUnlockCode').remove();
        return true;
    }
});

modal.addFooterBtn('Close', 'tingle-btn tingle-btn--danger', function() {
    
    modal.close();
    $('#unlockCode').remove();
	$('.sendUnlockCode').remove();

});

modal.addFooterBtn('Unlock Code', 'tingle-btn tingle-btn--primary', function() {
    
    //GUARDAMOS EL CODIGO DE DESBLOQUEO Y DAMOS POR CERRADO EL CASO
	var folio = $('#folioNo').html();
	var order = $('#orderNo').html();
	var code = $('#codeUnlock').val();

	if (code == 0) { code = ""; }

	$('.tingle-modal-box__footer').append('<input type="text" id="unlockCode" name="code" placeholder="Type Unlock Code Here" required>');
	$('#unlockCode').val(code);
	$('.tingle-modal-box__footer').append('<a class="tingle-btn tingle-btn--primary white-text sendUnlockCode">Send & Save it</a>');
    
    $('.sendUnlockCode').on('click',function() {

    	var unlockCode = $('#unlockCode').val();

    	if (unlockCode == "") {
    		M.toast({html: 'Unlock code its empty!<i class="material-icons right red-text text-darken-2">cancel</i>'});
    		$('#unlockCode').focus();
    		return false;
    	}

		$.ajax({
			type: 'post',
			url: 'php/orders/carga-unlock.php',
			data: {
		    	folio: folio,
		    	order: order,
		    	unlock: unlockCode
		    },
			dataType: "html",
    		context: document.body,
	        success: function(result) {
	        	modal.close();
	        	$('#unlockCode').remove();
	        	$('.sendUnlockCode').remove();
	        },

			error: function(xhr, tst, err) {
				console.log(err);
			}

		});

	})

});

//AQUI EMPIEZA LA ACCION
$(document).ready(function(){

	cargaTable();

});

function cargaTable(e) {

	$.ajax({
		url: 'php/unpaid/carga-tabla.php',
		contentType: "application/json",
        context: document.body,
        success: function(result) {
        	
        	//CARGAMOS TABLA
        	var table = $('#table').DataTable({

				data: result,
				dom: 'Bfrtip',
				buttons: [
			        'excel', 'pdf', 'print'
			    ],

			    //ROW CONDITIONS
			    'rowCallback': function(row, data, index){
				    if(data[9] == 0){
				        $(row).find('td:eq(9)').html('<i class="material-icons red-text text-lighten-2">payment</i>');
				    } else if (data[9] == 1){
				        $(row).find('td:eq(9)').html('<i class="material-icons green-text text-darken-1">payment</i>');
				    }
				    if(data[10] != ""){
				        $(row).find('td:eq(10)').html('<i class="material-icons green-text text-darken-2">done_all</i>');
				    } else {
				    	$(row).find('td:eq(10)').html('<i class="material-icons red-text text-lighten-2">cancel</i>');
				    }
				}
			    

			});

			//EVENTOS DE CLICK
			$('#table tbody').on('click', 'tr', function () {
				var data = table.row( this ).data();
				
				var modalContent = '<div class="row">';
				modalContent += '<div class="col s12 m4 offset-m2">';
				modalContent += '<p class="fiv-p"><i><em>Folio: </em></i><i class="right"><b id="folioNo">' + data[0] + '</b></i></p>';
				modalContent += '<p class="fiv-p"><i><em>Date: </em></i><i class="right"><b>' + data[1] + '</b></i></p>';
				modalContent += '<p class="fiv-p"><i><em>Client: </em></i><i class="right"><b>' + data[5] + '</b></i></p>';
				modalContent += '<p class="fiv-p"><i><em>Email: </em></i><i class="right grey-text text-darken-3"><b>' + data[4] + '</b></i></p>';
				modalContent += '<p class="fiv-p"><i><em>Order No.: </em></i><i class="right"><b id="orderNo">' + data[2] + '</b></i></p>';
				modalContent += '</div>';
				modalContent += '<div class="col s12 m4">';
				modalContent += '<p class="fiv-p"><i><em>IMEI: </em></i><i class="right grey-text text-darken-3"><b>' + data[3] + '</b></i></p>';
				modalContent += '<p class="fiv-p"><i><em>Company: </em></i><i class="right"><b>' + data[6] + '</b></i></p>';
				modalContent += '<p class="fiv-p"><i><em>Model: </em></i><i class="right"><b>' + data[7] + '</b></i></p>';
				modalContent += '<p class="fiv-p"><i><em>Cost: </em></i><i class="right"><b>$&nbsp;' + data[8] + '</b></i></p>';
				//STATUS Y COLOR DEL ESTATUS
				if (data[9] == 0) {var color = 'red-text text-lighten-2';} else if (data[9] == 1) { var color = 'green-text text-darken-1';}
				modalContent += '<p class="fiv-p"><i><em>Status: </em></i><i class="right"><i class="material-icons '+color+'">payment</i><b></b></i></p>';
				modalContent += '<input id="codeUnlock" type="hidden" value="' + data[10] + '">';
				modalContent += '</div>';
				modalContent += '</div>';

				//SET MODAL
				modal.setContent(modalContent);

				modal.open();

			} );



        },

		error: function(xhr, tst, err) {
			console.log(err);
		}

	});

}