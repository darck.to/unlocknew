$(document).ready(function(){

	$('.collapsible').collapsible();
	$('.sidenav').sidenav();

	$.ajax({
		url: 'template/dashboard/dashboard.php',
		dataType: "html",
		context: document.body,

		success: function(data) {

			$('#bodyContent').html(data);
			$('html, body').animate({ scrollTop: 0 }, 'fast');

		},

		error: function(xhr, tst, err) {
			console.log(err);
		}

	});

});