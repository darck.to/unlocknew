// instanciate new modal
var modal = new tingle.modal({
    stickyFooter: false,
    closeMethods: ['overlay', 'button', 'escape'],
    cssClass: ['custom-class-1', 'custom-class-2'],
    onOpen: function() {

    	//Opciones del editor de texto
		var editor = new MediumEditor('.editable', {
			imageDragging: false,
			toolbar: {
		    	buttons: ['bold', 'underline', 'h5', 'h6', 'quote', 'orderedlist'],// options go here
		    	disableExtraSpaces: true
			}
		});

    	//SAVE NEW BRAND
		$(".saveBrand").on("click", function(e) {
			var name = $('#nameModel').val();
			var type = $('#type').val();
			var form_data = new FormData();
		    form_data.append("name", name);
		    form_data.append("type", type);
		    form_data.append("file1", document.getElementById('fileModel').files[0]);
		    //GUARDAR BRAND
		    $.ajax({
		        type: 'post',
		        url: 'php/models/guarda-models.php',
		        data: form_data,
				cache: false,
		        contentType: false,
		        processData: false,
				dataType: "html",
		        context: document.body,
		        success: function(data) {

		        	M.toast({html: data});
		        	$('#modelCollection').html('');
		        	cargaModels();
		            modal.close();

		        },

		        error: function(xhr, tst, err) {
		            console.log(err);
		        }

		    });
		});

		//SAVE NEW EQUIPMENT
		$(".savePhone").on("click", function(e) {
			var fatherId = $('#fatherId').val();
			var name = $('#nameModel').val();
			var cost = $('#costModel').val();
			var type = $('#type').val();
			var desc = $('.editable').html();
			var form_data = new FormData();
			form_data.append("fatherId", fatherId);
			form_data.append("name", name);
			form_data.append("cost", cost);
			form_data.append("type", type);
			form_data.append("desc", desc);
			form_data.append("file1", document.getElementById('fileModel').files[0]);
			//GUARDAR BRAND
			$.ajax({
			    type: 'post',
			    url: 'php/models/guarda-models.php',
			    data: form_data,
				cache: false,
			    contentType: false,
			    processData: false,
				dataType: "html",
			    context: document.body,
			    success: function(data) {

			    	M.toast({html: data});
			    	$('#modelCollection').html('');
			    	cargaModels();
			        modal.close();

			    },

			    error: function(xhr, tst, err) {
			        console.log(err);
			    }

			});
		});

		//DELETE BUTTON
		$(".deleteButton").on("click", function(e) {
			var id = $(this).attr('id');
			var type = $(this).attr('type');
			var form_data = new FormData();
		    form_data.append("id", id);
		    form_data.append("type", type);

			//GUARDAR BRAND
		    $.ajax({
		        type: 'post',
		        url: 'php/models/borra-models.php',
		        data: form_data,
				cache: false,
		        contentType: false,
		        processData: false,
				dataType: "html",
		        context: document.body,
		        success: function(data) {

		        	M.toast({html: data});
		        	$('#modelCollection').html('');
		        	cargaModels();
		        	modal.close();
		        },

		        error: function(xhr, tst, err) {
		            console.log(err);
		        }

		    });
		});

		//DELETE BUTTON
		$(".portadaBrand").on("click", function(e) {
			var id = $(this).attr('id');
			var form_data = new FormData();
		    form_data.append("id", id);

			//GUARDAR BRAND
		    $.ajax({
		        type: 'post',
		        url: 'php/models/portada-models.php',
		        data: form_data,
				cache: false,
		        contentType: false,
		        processData: false,
				dataType: "html",
		        context: document.body,
		        success: function(data) {

		        	M.toast({html: data});
		        	$('#modelCollection').html('');
		        	cargaModels();
		        	modal.close();
		        },

		        error: function(xhr, tst, err) {
		            console.log(err);
		        }

		    });
		});

    },
    onClose: function() {
    },
    beforeClose: function() {
        return true;
    }
});

//AQUI EMPIEZA LA ACCION
$(document).ready(function(){

	cargaModels();

});

function cargaModels(e) {

	$.ajax({
		url: 'php/models/carga-models.php',
		contentType: "application/json",
        context: document.body,
        success: function(result) {

        	var lastId;
        	var starColor;

        	$('#firstBullet').html('<b class="handed addModel">ADD Brand</b>');

            $.each(result, function (name, value) {

            	if (value.portada == "1") {
            		starColor = "amber-text";
            	} else {
            		starColor = "grey-text";
            	}
          
    			$('#modelCollection').append("<li class='collection-item'>" + value.name + "<i class='material-icons left " + starColor + " handed starBrand' val=" + value.id + ">star</i><i class='material-icons right circle light-blue darken-4 white-text handed modelClick' val=" + value.id + " name='" + value.name + "'>chevron_right</i><i class='material-icons right red-text handed deleteBrand' val=" + value.id + " type=0>cancel</i><img class='right adm-model-img' src='../img/" + value.img + "'></li>")
    			lastId = value.id;

            });

            //CALL DELETE FUNCTION 
			$(".deleteBrand").on("click", function(e) {

				var id = $(this).attr('val');
				var type = $(this).attr('type');

				deletePhone(id, type);

			})

			//CLICKS MODELOS
			$(".starBrand").on("click", function(e) {

				var id = $(this).attr('val');

				var text = "<h4>Add to Home</h4>";
				text += "<div class='row grey lighten-4 border-r center-align'>";
				text += "<div class='col s12 input-field center-align'>";
				text += "<a class='tingle-btn tingle-btn--primary white-text portadaBrand' id="+id+">Save to Main Page</a>";
				text += "</div>";
				text += "</div>";

				// set content
            	modal.setContent(text);
				modal.open();

			});

            //CLICKS MODELOS
			$(".addModel").on("click", function(e) {

				var text = "<h4>Add model</h4>";
				text += "<div class='row'>";
				text += "<div class='col s12 m6 input-field'>";
				text += "<input id='nameModel' type='text'>";
				text += "<input id='type' type='hidden' value=0>";
				text += "<label for='nameModel'>Brand Name</label>";
				text += "</div>";
				text += "<div class='col s12 m6 file-field input-field'>"
				text += "<div class='btn'>"
				text += "<span>Brand Logo</span>"
				text += "<input id='fileModel' type='file' accept='image/*'>"
				text += "</div>"
				text += "<div class='file-path-wrapper'>"
				text += "<input class='file-path validate' type='text'>"
				text += "</div>"
				text += "</div>"
				text += "</div>"
				text += "<div class='row grey lighten-4 border-r'>";
				text += "<div class='col s12 m6 input-field'>";
				text += "<a class='tingle-btn tingle-btn--primary white-text saveBrand'>Save Brand</a>";
				text += "</div>";
				text += "</div>";

				// set content
            	modal.setContent(text);
				modal.open();

			});

			//CLICKS MODELOS
			$(".modelClick").on("click", function(e) {
				
				var id = $(this).attr('val');
				var name = $(this).attr('name');


				$.ajax({
					url: 'php/models/carga-equipos.php',
					contentType: "application/json",
			        context: document.body,
			        success: function(result) {

			        	$('#equipCollection').html('');

						$('#secBullet').html('<b class="handed addEquipo" val=' + id + '>ADD Model for Brand: ' + name + '</b>');

			            $.each(result, function (name, value) {


			            	if (value.idm == id) {

			    				$('#equipCollection').append("<li class='collection-item' val=" + value.id + ">" + value.name + "&nbsp;|&nbsp;<b>Cost: $" + value.cost + "</b><i class='material-icons right red-text handed deleteModel' val=" + value.id + " type=1>cancel</i><img class='right adm-model-img' src='../img/" + value.img + "'></li>");
			            	
			            	} else {
			            	}
			          

			            });

			            //CALL DELETE FUNCTION 
						$(".deleteModel").on("click", function(e) {

							var id = $(this).attr('val');
							var type = $(this).attr('type');

							deletePhone(id, type);

							$(this).parent('li').css('display','none');

						})

			            $('html, body').animate({ scrollTop: 0 }, 'fast');

			            //CLICKS MODELOS
						$(".addEquipo").on("click", function(e) {

							var fatherId = $(this).attr('val');

							var text = "<h4>Add Model</h4>";
							text += "<div class='row'>";
							text += "<div class='col s12 m6 input-field'>";
							text += "<input id='nameModel' type='text'>";
							text += "<input id='fatherId' type='hidden' value=" + fatherId + ">";
							text += "<input id='type' type='hidden' value=1>";
							text += "<label for='nameModel'>Model Name</label>";
							text += "</div>";
							text += "<div class='col s12 m6 input-field'>";
							text += "<input id='costModel' type='number'>";
							text += "<label for='costModel'>Unlock Cost</label>";
							text += "</div>";
							text += "<div class='col s12 m6 file-field input-field'>"
							text += "<div class='btn'>"
							text += "<span>Model Logo</span>"
							text += "<input id='fileModel' type='file' accept='image/*'>"
							text += "</div>"
							text += "<div class='file-path-wrapper'>"
							text += "<input class='file-path validate' type='text'>"
							text += "</div>"
							text += "</div>"
							text += "</div>"
							text += "<div class='row grey lighten-4 border-r'>"
							text += "<div class='editable text-editor'></div>"
							text += "</div>"
							text += "<div class='row grey lighten-4 border-r'>";
							text += "<div class='col s12 m6 input-field'>";
							text += "<a class='tingle-btn tingle-btn--primary white-text savePhone'>Save Brand</a>";
							text += "</div>";
							text += "</div>";

							// set content
			            	modal.setContent(text);
							modal.open();

						});

			        },

					error: function(xhr, tst, err) {
						console.log(err);
					}

				});

			});

        },

		error: function(xhr, tst, err) {
			console.log(err);
		}

	});

}

function deletePhone(i, t){

	var id = i;
	var type = t;

	var text = "<h4 class='center-align'>Confirm your Action</h4>";
	text += "<div class='row center-align grey lighten-3 border-r'>";
	text += "<div class='col s12 input-field'>";
	text += "<a class='tingle-btn tingle-btn--danger white-text center-align deleteButton' id="+id+" type="+type+"><h5><b>DELETE</b></h5></a>";
	text += "</div>";

	// set content
	modal.setContent(text);
	modal.open();

}