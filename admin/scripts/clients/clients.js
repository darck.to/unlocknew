// instanciate new modal
var modal = new tingle.modal({
    footer: true,
    stickyFooter: false,
    closeMethods: ['overlay', 'escape'],
    closeLabel: "Close",
    cssClass: ['custom-class-1', 'custom-class-2'],
    onOpen: function() {
    },
    onClose: function() {
    },
    beforeClose: function() {
        return true;
    }
});

modal.addFooterBtn('Close', 'tingle-btn tingle-btn--danger', function() {
    
    modal.close();

});

//AQUI EMPIEZA LA ACCION
$(document).ready(function(){

	cargaTable();

});

function cargaTable(e) {

	$.ajax({
		url: 'php/clients/carga-tabla.php',
		contentType: "application/json",
        context: document.body,
        success: function(result) {

        	//CARGAMOS TABLA
        	var table = $('#table').DataTable({

				data: result,
				dom: 'Bfrtip',
				buttons: [
			        'excel', 'pdf', 'print'
			    ],			    

			});

			//EVENTOS DE CLICK
			$('#table tbody').on('click', 'tr', function () {
				var data = table.row( this ).data();
				
				var modalContent = '<div class="row">';
				modalContent += '<div class="col s12 m4 offset-m2">';
				modalContent += '<p class="fiv-p"><i><em>Folio: </em></i><i class="right"><b id="folioNo">' + data[0] + '</b></i></p>';
				modalContent += '<p class="fiv-p"><i><em>Date: </em></i><i class="right"><b>' + data[1] + '</b></i></p>';
				modalContent += '<p class="fiv-p"><i><em>Client: </em></i><i class="right"><b>' + data[5] + '</b></i></p>';
				modalContent += '<p class="fiv-p"><i><em>Email: </em></i><i class="right grey-text text-darken-3"><b>' + data[4] + '</b></i></p>';
				modalContent += '<p class="fiv-p"><i><em>Order No.: </em></i><i class="right"><b id="orderNo">' + data[2] + '</b></i></p>';
				modalContent += '</div>';
				modalContent += '<div class="col s12 m4">';
				modalContent += '<p class="fiv-p"><i><em>IMEI: </em></i><i class="right grey-text text-darken-3"><b>' + data[3] + '</b></i></p>';
				modalContent += '<p class="fiv-p"><i><em>Company: </em></i><i class="right"><b>' + data[6] + '</b></i></p>';
				modalContent += '<p class="fiv-p"><i><em>Model: </em></i><i class="right"><b>' + data[7] + '</b></i></p>';
				modalContent += '<p class="fiv-p"><i><em>Cost: </em></i><i class="right"><b>$&nbsp;' + data[8] + '</b></i></p>';
				//STATUS Y COLOR DEL ESTATUS
				if (data[9] == 0) {var color = 'red-text text-lighten-2';} else if (data[9] == 1) { var color = 'green-text text-darken-1';}
				modalContent += '<p class="fiv-p"><i><em>Status: </em></i><i class="right"><i class="material-icons '+color+'">payment</i><b></b></i></p>';
				modalContent += '<input id="codeUnlock" type="hidden" value="' + data[10] + '">';
				modalContent += '</div>';
				modalContent += '</div>';

				//SET MODAL
				modal.setContent(modalContent);

				modal.open();

			} );



        },

		error: function(xhr, tst, err) {
			console.log(err);
		}

	});

}