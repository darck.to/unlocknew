<div class="row">

	<div class="row">
		
		<div class="col s12">
			<span class="admin-title"><?php echo _("Unpaid Clients"); ?></span><span class="admin-bullets">&nbsp;<i class="blue title-bullets"></i><i class="amber title-bullets"></i></span>
		</div>

	</div>
	
	<div class="col s12 m10 offset-m1">
		
		<table id="table" class="display" style="width:100%">
	        <thead>
	            <tr>
	                <th>Folio</th>
	                <th>Date</th>
	                <th>Order ID</th>
	                <th>IMEI</th>
	                <th>Email</th>
	                <th>Customer</th>
	                <th>Company</th>
	                <th>Model</th>
	                <th>Cost</th>
	                <th>Status</th>
	                <th>Code</th>
	            </tr>
	        </thead>
	        <tfoot>
	            <tr>
	                <th>Folio</th>
	                <th>Date</th>
	                <th>Order ID</th>
	                <th>IMEI</th>
	                <th>Email</th>
	                <th>Customer</th>
	                <th>Company</th>
	                <th>Model</th>
	                <th>Cost</th>
	                <th>Status</th>
	                <th>Code</th>
	            </tr>
	        </tfoot>
	    </table>

	</div>

</div>

<script type="text/javascript" src="scripts/unpaid/unpaid.js"></script>