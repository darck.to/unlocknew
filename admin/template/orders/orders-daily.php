<div class="row">

	<div class="row">
	
		<div class="col s12">
			<span class="admin-title"><?php echo _("Orders Daily"); ?></span><span class="admin-bullets">&nbsp;<i class="blue title-bullets"></i><i class="amber title-bullets"></i></span>
		</div>

	</div>
	
	<div class="col s12 m10 offset-m1">
		
		<div class="card z-depth-0">
			<div class="card-content">
				<div class="row">
					<div id='daily' class="col s12"></div>
				</div>
			</div>
		</div>
		
	</div>

</div>

<script type="text/javascript" src="scripts/orders/orders-daily.js"></script>