<?php
	
	header('Content-type: application/json');

	$resultados = array();

	//NOMBRE DE ARCHIVO
	$fileList = glob('../../assets/*order*.json');

	//RECORREMOS LOS ARCHIVOS
	foreach($fileList as $filename){

	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
		if (file_exists($filename)) {
			
			$filename = file_get_contents($filename);
			$json = json_decode($filename, true);

			foreach ($json as $content) {

				$resultados[] = array($content['folio'], $content['fecha'], $content['random'], $content['imei'], $content['email'], $content['nombre'], $content['marca'], $content['equipo'], $content['precio'], $content['estatus'], $content['code']);

			}

		}

	}

	print json_encode($resultados);

?>