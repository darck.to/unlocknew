<?php
	
	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	include('../../func/abre_conexion.php');

	//RECIBIMOS LA INFORMACION
	$folio = mysqli_real_escape_string($mysqli, $_POST['folio']);
    $order = mysqli_real_escape_string($mysqli, $_POST['order']);
    $unlock = mysqli_real_escape_string($mysqli, $_POST['unlock']);

	$jsonString = file_get_contents('../../assets/'.$folio.'_order_'.$order.'.json');
	$data = json_decode($jsonString, true);

	$data[0]['estatus'] = 1;
	$data[0]['code'] = $unlock;

	//LO VOLVEMOS A GUARDAR
	$newJsonString = json_encode($data);
	file_put_contents('../../assets/'.$folio.'_order_'.$order.'.json', $newJsonString);

    include('../../func/cierra_conexion.php');

    //ENVIAMOS EL MAIL DE BIENVENIDA//
	//////////////////////////////////
	$asunto = "Orden Procesada";
	$mensaje = "<h1 style='text-align: center; font-weight: lighter;'>iUNLOCK MX</h1>
				<p><h2>Tu orden <b>".$order."</b> ha sido procesada</h2></p>
				<p><h3>Tu codigo de desbloqueo es: <b>".$unlock."</b></h3></p>
				<div style='display: block; padding: 5px 0;'></div>
				<p>Te avisaremos en cuanto este lista tu orden</p>
				<div style='display: block; padding: 5px 0;'></div>
				<p>Revisa tu orden en nuestra p&aacute;gina <b><a href='http://unlock.com.mx' target:'_blank'>AQUI</a></b></p>
				<div style='display: block; padding: 10px 0;'></div>
				<p>Si has recibido este mensaje y no eres el administrador de la cuenta, contactate con servicio técnico de Lizipar para poder auxiliarte: <a href='mailto:serviciotecnico@lizipar.com.mx' style='color: #039be5; text-decoration: none; -webkit-tap-highlight-color: transparent; color: #4b4b4b; font-weight: bolder;'><b>AQUI</b></a></p>
	";
	include_once('../../func/mailFunctions.php');
	enviaCorreo($asunto,$mensaje,$email);

?>