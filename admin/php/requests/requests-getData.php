<?php
	header('Content-type: application/json');
	
	//NOMBRE DE ARCHIVO
	$fileList = glob('../../assets/*check*.json');

	//RECORREMOS LOS ARCHIVOS
	foreach($fileList as $filename){

	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
		if (file_exists($filename)) {
			
			$filename = file_get_contents($filename);
			$json = json_decode($filename, true);

			foreach ($json as $content) {

				//GUARDAMOS EL ARCHIVO DE LA NOTA
				$cabecera[] = array('title'=> $content['email']." - ".$content['nombre'], 'start'=> $content['fecha'], 'code'=> $content['folio'].'_check_'.$content['random']);

			}

		}

	}

	echo json_encode($cabecera);

?>