<?php
	
	header('Content-type: application/json');

	$code = $_POST['code'];

	$cabecera = array();

	$file = '../../assets/'.$code.'.json';
	
   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
	if (file_exists($file)) {
		
		$filename = file_get_contents($file);
		$json = json_decode($filename, true);

		foreach ($json as $content) {

			//GUARDAMOS EL ARCHIVO DE LA NOTA
			$cabecera = array('random'=> $content['random'], 'fecha'=> $content['fecha'], 'imei'=> $content['imei'], 'usuario'=> $content['nombre'], 'email'=> $content['email'], 'precio'=> $content['precio'], 'comentario'=> $content['comentario'], 'estatus'=> $content['estatus'], 'code'=> $content['code']);

		}

	}

	echo json_encode($cabecera);

?>