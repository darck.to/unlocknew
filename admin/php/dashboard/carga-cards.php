<?php

	header('Content-type: application/json');

	$resultados = array();
	$imeiOrders = 0;
	$imeiCompleted = 0;
	$mails = array();
	$comments = array();
	$checkOrders = 0;
	$checkCompleted = 0;
	
	//NOMBRE DE ARCHIVO
	$fileList = glob('../../assets/*order*.json');

	//RECORREMOS LOS ARCHIVOS
	foreach($fileList as $filename){

	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
		if (file_exists($filename)) {
			
			$filename = file_get_contents($filename);
			$json = json_decode($filename, true);

			foreach ($json as $content) {

				$mails[] = $content['email'];

				$imeiOrders++;

				if ($content['estatus'] == 1) {
					$imeiCompleted ++;
					$comments[] = $content['comentario'];
				}


			}

		}

	}

	//NOMBRE DE ARCHIVO
	$fileList = glob('../../assets/*check*.json');

	//RECORREMOS LOS ARCHIVOS
	foreach($fileList as $filename){

	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
		if (file_exists($filename)) {
			
			$filename = file_get_contents($filename);
			$json = json_decode($filename, true);

			foreach ($json as $content) {

				$mails[] = $content['email'];

				$checkOrders++;

				if ($content['estatus'] == 1) {
					$checkCompleted ++;
					$comments[] = $content['comentario'];
				}


			}

		}

	}

	$resultados[] = $imeiOrders;
	$resultados[] = $imeiCompleted;

	$resultados[] = count($mails);
	$resultados[] = count(array_unique($mails));

	$resultados[] = count($comments);

	$resultados[] = $checkOrders;
	$resultados[] = $checkCompleted;

	print json_encode($resultados);

?>