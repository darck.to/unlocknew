<footer class="page-footer grey lighten-2">

	<div class="grey-text text-darken-2 footer-copyright">
		
		<div class="container">
			<?php echo _("Panel de administraci&oacute;n y Reportes"); ?>
			<i class="right">iUnlockMx</i>
		</div>

	</div>

</footer>