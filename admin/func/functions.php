<?php	

	//GENERADOR DE CADENAS ALEATORIAS
	function generateRandomString($length = 16) {
	    $characters = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	//CAMBIAMOS PESO DE LAS IMAGENES
	function compress($source, $destination, $quality) {

		$info = getimagesize($source);

		if ($info['mime'] == 'image/jpeg') 
		  $image = imagecreatefromjpeg($source);
		elseif ($info['mime'] == 'image/gif') 
		  $image = imagecreatefromgif($source);
		elseif ($info['mime'] == 'image/png') 
		  $image = imagecreatefrompng($source);

		imagejpeg($image, $destination, $quality);

		return $destination;
	}

	function fileImag($file, $imgname, $phone) {

	    if(isset($file)) {
		    $errors= array();
		    $file_tmp =$_FILES['file1']['tmp_name'];
		    $file_name =$_FILES['file1']['name'];
		    $file_ext = end((explode(".", $file_name)));
		    //CAMBIAMOS EL NOMBRE DEL ARCHIVO A UNO MAS CORTO Y DE ACUERDO A LA NOMENCLATURA DEL IDATT
		    $file_name = "logos/".$phone."logo-".$imgname.".".$file_ext;
		    $extensions= array($file_ext);
		    
		    if(in_array($file_ext,$extensions)=== false){
		      $errors[]="extension not allowed, please choose a JPEG or PNG file.";
		    }

		    if(empty($errors)==true){
		      //LE CAMBIAMOS EL TAMAÑO A LA IMAGEN ANTES DE GUARDARLA EN LA BDD
		      $file_name_final_1 = "../../../img/".$file_name;
		      $source_img = $file_tmp;
		      $destination_img = $file_name_final_1;
		      //LLAMAMOS LA FUNCION PARA CAMBIARLE EL TAMAÑO
		      $file_compressed = compress($source_img, $destination_img, 80);
		      //LA GUARDAMOS EN EL DIRECTORIO CORRESPONDIENTE
		      move_uploaded_file($file_compressed,".".$file_name);
		      //echo "Imagen Subida!";
		      return $file_name;
		    }else{
		      print_r($errors);
		    }
		} else {
		  	//echo "Ocurrio un error, favor de consultar al administrador";
		  	$file_name_final_1 = "";
		}

    }

    function readLastId($filename) {
		if (file_exists($filename)) {
		
			$filename = file_get_contents($filename);
			$json = json_decode($filename, true);

			$last = end($json);

			$id = $last['id'];

			return $id;

		} else {

		}		
	}

	function leeFolio() {
		//NOMBRE DE ARCHIVO
		$filename = '../../admin/assets/folio.json';
		if (file_exists($filename)) {
			$filename = file_get_contents($filename);
			$data = json_decode($filename, true);
			$folio = $data[0]['folio'];
		}
		return $folio;
	}

	function masFolio() {
		$filename = file_get_contents('../admin/assets/folio.json');
		$data = json_decode($filename, true);
		$viejo = $data[0]['folio'];
		$data[0]['folio'] = $viejo + 1;
		//LO VOLVEMOS A GUARDAR
		$newJsonString = json_encode($data);
		file_put_contents('../../admin/assets/folio.json', $newJsonString);
	}

	//LIMPIAMOS LAS VARIABLES
	function html_escape($html_escape) {
        $html_escape =  htmlspecialchars($html_escape, ENT_QUOTES | ENT_HTML5, 'UTF-8');
        return $html_escape;
    }

?>