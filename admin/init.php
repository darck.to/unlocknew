<!-- jQuery -->
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<script type="text/javaScript" src="js/jquery-ui.min.js"></script>

<!--JavaScript at end of body for optimized loading-->
<script type="text/javascript" src="js/materialize.min.js"></script>

<!--Medium Editor-->
<script src="js/medium-editor.min.js"></script>
<script src="js/custom-html.js"></script>

<!--Charts-->
<script src="js/Chart.bundle.js"></script>

<!--Modal-->
<script src="js/tingle.js"></script>

<!--DataTable-->
<script type="text/javascript" src="js/datatables.js"></script>
<script type="text/javascript" src="js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="js/buttons.flash.min.js"></script>
<script type="text/javascript" src="js/buttons.html5.min.js"></script>
<script type="text/javascript" src="js/buttons.print.min.js"></script>
<script type="text/javascript" src="js/jszip.min.js"></script>
<script type="text/javascript" src="js/pdfmake.min.js"></script>
<script type="text/javascript" src="js/vfs_fonts.js"></script>

<!--Full Calendar-->
<script type="text/javaScript" src="js/jquery-ui.min.js"></script>
<script type="text/javaScript" src="js/moment.min.js"></script>
<script type="text/javaScript" src="js/fullcalendar.js"></script>

<!-- Load -->
<script type="text/javaScript" src="init/login.js"></script>
<script type="text/javaScript" src="scripts/menu.js"></script>
<script type="text/javascript" src="scripts/search/search.js"></script>
<script type="text/javaScript" src="scripts/adminLoad.js"></script>