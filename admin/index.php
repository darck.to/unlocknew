<!-- seccion de footer-->
<?php include_once('headers.php'); ?>

<body>

	<!-- Body -->

	<!-- seccion de logo-->
	<div class="row no-margin-bottom">

		<?php include_once('menu.php'); ?>

	</div>


	<div class="row" id="loginDiv">
	</div>

	<main class="p-r">
		
		<!-- Show Results -->
		<div id="results-container" class="row hide p-a">
			<ul class="col s12 z-depth-1 white handed no-p" id="results"></ul>
		</div>
		
		<div id="bodyContent" class="row">

		</div>

	</main>
	
	<!-- seccion de footer-->
	<?php include_once('footer.php'); ?>

</body>

</html>

<?php include_once('init.php'); ?>