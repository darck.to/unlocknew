<?php
	session_start();

  	if (isset($_SESSION['usuario_nombre'])) {
?>

<div class="fullWrapper">

	<div class="loguinWrapper col s12 m12 l12">

		<div class="row">

		    <div class="col s12 m6 l4 offset-m3 offset-l4">

		    	<div class="card grey lighten-5">
		    	
		    		<i class="material-icons right close-login handed">close</i>

		    		<div class="card-content white">
						
						<span class="card-title">
							<i class="material-icons left">account_circle</i>
							<p>Cierra sesión</p>
						</span>

					</div>

					<div class="card-action white">
					
						<div class="row center-align">

							<a class="waves-effect waves-light btn-large red" name="cerrar" href="init/logout.php"><i class="material-icons left">play_arrow</i>Cerrar Sesión</a>
							
						</div>

					</div>

		    	</div>

		    </div>

		</div>

	</div>

</div>

<?php
	} else {

?>

<div class="fullWrapper">

	<div class="loguinWrapper col s12 m12 l12">

		<div class="row">

		    <div class="col s12 m6 l4 offset-m3 offset-l4">

		    	<div class="card grey lighten-5">

		    		<i class="material-icons right close-login handed">close</i>

		    		<div class="card-content white">
						
						<span class="card-title">
							<i class="material-icons left">account_circle</i>
							<p>Inicia sesión</p>
						</span>

					</div>

					<div class="card-action white">
					
						<div class="row">

							<form class="col s12 m10 l10 offset-m1 offset-l1" id="formLogin" name="formLogger" action="init/comprueba.php" method="post" autocomplete="off">

								<input class="form-control" type="text" name="usuario_nombre" placeholder="Usuario" required autocomplete="off">
								<input class="form-control" type="password" name="usuario_clave" placeholder="Contraseña" required autocomplete="off">
								<button class="waves-effect waves-light btn blue-grey" name="enviar"><i class="material-icons left">play_arrow</i>Iniciar Sesión</button>
								
							</form>

						</div>

						<span onclick="resetPass()" class="handed">Recuperar Contraseña</span>
						
						<div id="recuperaPass">
						</div>

					</div>
					
					<div class="card-content teal darken-1 white-text">

						<span class="card-title" id="aviso">
						</span>

						<a class="waves-effect waves-light btn-small right" onclick="createAccount()"><i class="material-icons right">add</i>Crea una Cuenta</a>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<?php
	}
?>

<script type="text/javaScript" src="scripts/loginDo.js"></script>