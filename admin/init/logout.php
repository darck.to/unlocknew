<?php
  session_start();

  include ('abre_conexion.php'); // incluímos los datos de acceso a la BD

  // comprobamos que se haya iniciado la sesión
  if(isset($_SESSION['usuario_nombre'])) {
    session_destroy();
    echo "Sesion Cerrada
          <script>
            window.location.href = \"../\";
          </script>";
  }else {
    session_destroy();
    echo "Operacion Incorrecta
          <script>
            window.location.href = \"../\";
          </script>";
  }

  include ('cierra_conexion.php');
?>