<!--CINTILLO DE PAGOS-->
<div class="has-background-white">
    <div class="container">
        <nav class="level">
            <div class="level-left">
                <div class="level-item">
                    <p class="has-text-weight-bold font-color-1">FORMAS DE PAGO</p>
                </div>
            </div>
            <div class="level-right">
                <div class="level-item">
                    <p class="has-text-weight-bold font-color-1">PAGOS SEGUROS</p>
                </div>
            </div>
        </nav>
    </div>
</div>

<!--FOOTER-->
<footer class="footer has-background-black">
  <div class="container content">
    <div class="columns">
    	<div class="column">
    		<h5 class="has-text-white"><?php echo _("MARCAS DESTACADAS"); ?></h5>
    		<ul id="menuFooter">
    		</ul>
    	</div>
    	<div class="column">
    		<h5 class="has-text-white"><?php echo _("MARCAS POPULARES"); ?></h5>
    		<ul>
    			<li>Contenido Aqu&iacute</li>
    			<li>Contenido Aqu&iacute</li>
    			<li>Contenido Aqu&iacute</li>
    			<li>Contenido Aqu&iacute</li>
    		</ul>
    	</div>
    	<div class="column">
    		<h5 class="has-text-white"><?php echo _("NAVEGACI&Oacute;N"); ?></h5>
    		<ul>
    			<li><?php echo _("REVISAR IMEI"); ?></li>
    			<li><?php echo _("CUPONES"); ?></li>
    			<li><?php echo _("PREGUNTAS A EXPERTOS"); ?></li>
    			<li><?php echo _("CONTACTO"); ?></li>
    		</ul>
    	</div>
    	<div class="column">
    		<h5 class="has-text-white"><?php echo _("TITULO DEL MEN&Uacute;"); ?></h5>
    		<ul>
    			<li>Contenido Aqu&iacute</li>
    			<li>Contenido Aqu&iacute</li>
    			<li>Contenido Aqu&iacute</li>
    			<li>Contenido Aqu&iacute</li>
    		</ul>
    	</div>
    </div>
  </div>
</footer>