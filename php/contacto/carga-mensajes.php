<?php
	
	header('Content-type: application/json');
	
   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
	if (file_exists('files/mensajes.json')) {
		
		$filename = file_get_contents('files/mensajes.json');
		$json = json_decode($filename, true);

	}

	echo json_encode($json);

?>