<?php
	
	header('Content-type: application/json');
	
   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
	if (file_exists('files/respuestas.json')) {
		
		$filename = file_get_contents('files/respuestas.json');
		$json = json_decode($filename, true);

	}

	echo json_encode($json);

?>