<?php
	
	header('Content-type: application/json');
	
   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
	$fileList = glob('../../admin/assets/*_order_*.json');
	shuffle($fileList);

	$results = array();

	//RECORREMOS LOS ARCHIVOS
	foreach($fileList as $filename){

	   	//SI SOY ARCHIVOS JSON LOS LEEMOS PARA MOSTRARLOS
		if (file_exists($filename)) {
			
			$filename = file_get_contents($filename);
			$json = json_decode($filename, true);

			foreach ($json as $content) {

				if ($content['estatus'] == 1) {
					$results[] = $content['nombre'];
					$results[] = $content['equipo'];
					$results[] = $content['comentario'];
					$results[] = $content['fecha'];
				}

			}

		}
	}


	echo json_encode($results);

?>