<?php

	include_once('../../func/functions.php');

	if (empty($_POST['imei'])) {die("Falta el IMEI"); }
	if (empty($_POST['email'])) {die("Falta el Email"); }

	//LEEMOS EL ULTIMO FOLIO
	$folio = leeFolio();

	//RECIBIMOS LA INFORMACION
	$imei = html_escape($_POST['imei']);
	$email = html_escape($_POST['email']);

    date_default_timezone_set("America/Mexico_City");
	$fecha = Date('Y-m-d H:i');
	$mes = Date('m');

	//ID ALEATORIO
	$random = generateRandomString();

	//GUARDAMOS EL ARCHIVO DE LA NOTA
    $cabecera[] = array('folio'=> $folio,'fecha'=> $fecha, 'mes'=> $mes, 'random'=> $random, 'imei'=> $imei, 'email'=> $email, 'nombre'=> 'IMEI Request', 'precio'=> 0, 'comentario'=> '', 'estatus' => 0, 'code' => 0);

	//NOMBRE DE ARCHIVO
	$fileName = '../../admin/assets/'.$folio.'_check_'.$random.'.json';
	//INCREMENTAMOS EL FOLIO
	masFolio();
	if (file_exists($fileName)) {
	} else {
		$fileFinal = fopen($fileName, 'w') or die ('No se guardo el archivo \n');
		fwrite($fileFinal, json_encode($cabecera, JSON_PRETTY_PRINT));
		fclose($fileFinal);
		echo $random."\n";
	}

	//ENVIAMOS EL MAIL DE BIENVENIDA//
	//////////////////////////////////
	$asunto = "Solicitud en Proceso";
	$mensaje = "<h1 style='text-align: center; font-weight: lighter;'>iUNLOCK MX</h1>
				<p><h2>Guarda tu n&uacute;mero de orden:</h2></p>
				<p><h3><b>".$random."</b></h3></p>
				<div style='display: block; padding: 5px 0;'></div>
				<p>Te avisaremos en cuanto tengamos respuesta de tu Solicitud</p>
				<div style='display: block; padding: 5px 0;'></div>
				<p>Revisa tu orden en nuestra p&aacute;gina <b><a href='http://unlock.com.mx' target:'_blank'>AQUI</a></b></p>
				<div style='display: block; padding: 10px 0;'></div>
				<p>Si has recibido este mensaje y no eres el administrador de la cuenta, contactate con servicio técnico de iUnlockMX para poder auxiliarte: <a href='mailto:serviciotecnico@iunlock.com.mx' style='color: #039be5; text-decoration: none; -webkit-tap-highlight-color: transparent; color: #4b4b4b; font-weight: bolder;'><b>AQUI</b></a></p>
	";
	include_once('../../func/mailFunctions.php');
	enviaCorreo($asunto,$mensaje,$email);

?>