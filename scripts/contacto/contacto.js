//PLANTILLAS CAJAS DE MENSAJES
var boxMensajes = '<div class="row no-m-bot single-item"><div class="col s12"><div class="card z-depth-1 border-r-10"><div class="row"><div class="col s12">';
var end = '</div></div></div></div></div>';

// instanciate new modal
var modal = new tingle.modal({
    footer: true,
    stickyFooter: false,
    closeMethods: ['escape'],
    closeLabel: "Close",
    cssClass: ['custom-class-1', 'custom-class-2'],
    onOpen: function() {
    },
    onClose: function() {
    },
    beforeClose: function() {
        return true;
    }
});

modal.addFooterBtn('Cerrar', 'tingle-btn tingle-btn--primary', function() {
    
    modal.close();                

});

$(document).ready(function(){

    //CARGA MENSAJES
    cargaMensajes();

    //CARGA FOOTER
    cargaMarcasFooter()

});

function cargaMensajes(e) {

    var respuestas;

    //SOLICITAMOS LAS RESPUESTAS
    $.ajax({

        method: 'post',
        url: 'php/contacto/carga-respuestas.php',
        dataType: "json",
        context: document.body,
        success: function(data) {
            respuestas = data
        },
        error: function(xhr, tst, err) {
            console.log(err)
        }

    });
    //SOLICITAMOS LOS MENSAJES
    $.ajax({

        method: 'post',
        url: 'php/contacto/carga-mensajes.php',
        dataType: "json",
        context: document.body,
        success: function(data) {

            data.sort(function(a, b){return b.id - a.id});

            $.each(data, function(index, message) {
                $('#contactoMensajes').append(boxMensajes + '<div class="card-content no-p-b"><h6 class="blue-text text-lighten-1 font-w-br">' + message.usuario + '</h6></div><div class="card-content no-p-t"><div class="row"><p class="grey-text line-h-25 word-s-6 searchParagraph"><small class="green-text">' + message.fecha + '</small>&nbsp;|&nbsp;<b>"' + message.mensaje + '"</b></p><div class="divider ten-m"></div><p class="right right-align line-h-25 word-s-6 p' + index + '"></p></div></div>' + end);
                $.each(respuestas, function(pointer, respuestas) {
                    if (respuestas.id_m == message.id) {
                        $('.p' + index + '').html("<b class='green-text'>" + respuestas.mensaje + "</b>&nbsp;|&nbsp;<small class='grey-text'>" + respuestas.fecha + "</small>");
                    }
                });
            });

            //PAJINATOR
            $(".container-pajinator").pagify(5, ".single-item");

        },
        error: function(xhr, tst, err) {
            console.log(err);
        }

    });

};

//CARGA FOOTER
function cargaMarcasFooter(e) {

    $.ajax({
        type: "POST",
        url: "php/home/carga-marcas.php",
        dataType: "json",
        context: document.body,

        success: function(data) {
            console.log(data);

            $.each(data, function(index, home) {
                if (home.portada == 1) {
                    $('#menuFooter').append('<li class="marcaFooter handed" val=' + home.id + ' mar=' + home.name + '><a class="white-text">' + home.name + '</a></li>');
                }
            });

            //CARGA LA MARCA SELECCIONADA
            $('.marcaFooter').on('click',function(){

                var val = $(this).attr('val');
                var marca = $(this).attr('mar');

                //cargaEquipos(val, marca);
                cargaImei(val, marca);

            })
        },

        error: function(xhr, tst, err) {
            console.log(err);
        }

    });

};

//CARGA IMEI ULTIMO PASO ANTES DEL PAGO
function cargaImei(id, mar) {

    var id = id;
    var marca = mar;

    var url = 'desbloquear.php';
    var form = $('<form action="' + url + '" method="POST" class="hide">' +
    '<input type="text" name="paso" value=1 />' +
    '<input type="text" name="id" value="' + id + '" />' +
    '<input type="text" name="marca" value="' + marca + '" />' +
    '</form>');
    $('body').append(form);
    form.submit();


}

//OMNI Buscador
$("input.omniSearch").on("keyup", function(e) {
    
    var input, filter, parent, contenedor, p, i;

    input = $(this);
    
    filter = input.val().toUpperCase();

    parent = $('#contactoMensajes > .single-item');

    contenedor = $('.col > .card > .row > .col > .no-p-t > .row > .searchParagraph')
    
    for (i = 0; i < contenedor.length; i++) {
        
        p = contenedor[i].getElementsByTagName("b")[0];
        
        if (p.innerHTML.toUpperCase().indexOf(filter) > -1) {

            parent[i].style.display = "";

        } else {

            parent[i].style.display = "none";

        }

    }

});