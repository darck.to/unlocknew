//EQUIPOS
var boxEquipos = '<div class="tile is-parent is-2 has-background-white marca-encajonada marca-equipos-box shadow-blue handed"><div class="equipoName">';
var end = '</div></div>';
//TIPO DE PAGO
var pago;

//VARIABLES
var imei;
var email;
var name;
var logoUnlock;
var marca;
var equipo;
var precio;
var desc;
var pais;
var operator;

//NUMERO DE ORDER 
var order;

// instanciate new modal
var modal = new tingle.modal({
    footer: true,
    stickyFooter: false,
    closeMethods: ['overlay','button','escape'],
    closeLabel: "Close",
    cssClass: ['custom-class-1', 'custom-class-2'],
    onOpen: function() {
    },
    onClose: function() {
    },
    beforeClose: function() {
        return true;
    }
});

modal.addFooterBtn('Confirm your Order', 'tingle-btn tingle-btn--primary', function() {
    
    if (pago == 0) {

        $('.tingle-modal-box__footer').append('<div id="paypal-button"></div>');

        //CARGAMOS PAYPAL
        jQuery.getScript("scripts/desbloquea/checkOutPaypal.js", function(data, status, jqxhr) {
        });


    } else if (pago == 1) {
        
        //PROCESA PAGO
        var form_data = new FormData();

        form_data.append("imei", imei);
        form_data.append("email", email);
        form_data.append("nombre", name);
        form_data.append("marca", marca);
        form_data.append("equipo", equipo);
        form_data.append("precio", precio);
        form_data.append("pais", pais);
        form_data.append("operator", operator);

        //SOLICITAMOS EL CODIGO
        $.ajax({

            method: 'post',
            url: 'php/desbloquea/desbloquea-order.php',
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "html",
            context: document.body,
            success: function(data) {

                modal.close();
                
                $('#orderDiv').html('').delay(2000);
                $('#orderDiv').addClass('is-hidden');
                $('#orderDiv').addClass('border-r-10 orange lighten-3 ten-p');
                $('#orderDiv').append('<h4 class="center-align">Tu n&uacute;mero de orden es: <b>' + data + '</b></h4>');
                $('#orderDiv').append('<h5 class="center-align">Guardalo para futuras referencias</h5>');
                $('#orderDiv').removeClass('is-hidden');

            },
            error: function(xhr, tst, err) {
                console.log(err);
            }

        });

    }

});

$(document).ready(function(){

    //CARGA MENU
    cargaMenu();

    //CARGA EQUIPOS
    cargaEquipos();

    //CARGA SELECTS
    cargaSelects();

    //CARGA CAPTURAIMEI
    cargaCapturaImei();

    //CARGA FOOTER
    cargaMarcasFooter()

});

function cargaCapturaImei(e) {

	id = $('#id').html();
    marca = $('#marca').html();
    equipo = $('#equipo').html();

    //SOLICITAMOS LA MARCA
    $.ajax({

        method: 'post',
        url: 'php/desbloquea/carga-equipos.php',
        dataType: "json",
        context: document.body,
        success: function(data) {

            $.each(data, function(index, unlock) {
                if (unlock.id == equipo) {
                    equipo = unlock.name;
                    precio = unlock.cost;
                    desc = unlock.desc;
                    logoUnlock = unlock.img;
                }
            });

        	$(".marca-brand").attr('src','img/' + logoUnlock);
            $("#marcaBrand").html(marca);
            $("#nameEquipment").html(equipo);
        	$("#equipoBrand").html(equipo);
            $("#descPhone").html(desc);
            $(".precioDesbloqueo").html(precio);
            $("#sectionMarcaEquipo").html(marca + ' ' + equipo);
            $("#sectionImgEquipo").attr('src','img/' + logoUnlock);
            $(".sectionInstrucciones").removeClass('is-hidden');

        },
        error: function(xhr, tst, err) {
            console.log(err);
        }

    });

};

//CARGA SELECTS DE PAISES Y OPERADORES
function cargaSelects(e) {

    $('.thirdStepOrder').addClass('is-hidden');


    //CARGA LOS PAISES DESDE JSON
    $.ajax({

        method: 'post',
        url: 'php/desbloquea/carga-paises.php',
        dataType: "json",
        context: document.body,
        success: function(data) {

            $.each(data, function(index, pais) {
                $("#selectCountry").append("<option class='cargaOperador' value=" + pais.id + ">" + pais.pais + "</option>");
            });
            
            $('#selectCountry').on('change',function(){
                $("#selectOperator").html("");
                var paisSelect = $(this).val();
                //CARGA LOS OPERADORES DESDE JSON
                $.ajax({

                    method: 'post',
                    url: 'php/desbloquea/carga-operators.php',
                    dataType: "json",
                    context: document.body,
                    success: function(data) {

                        $.each(data, function(index, operator) {
                            if (operator.id_m == paisSelect) {
                                $("#selectOperator").append("<option value=" + operator.id + ">" + operator.operador + "</option>");
                            }
                        });

                        $('.thirdStepOrder').removeClass('is-hidden');

                    },
                    error: function(xhr, tst, err) {
                        console.log(err);
                    }

                });
            })

        },
        error: function(xhr, tst, err) {
            console.log(err);
        }

    });

}

function cargaEquipos() {

    id = $('#id').html();
    marca = $('#marca').html();

    $.ajax({
        type: "POST",
        url: "php/desbloquea/carga-equipos.php",
        dataType: "json",
        context: document.body,
        success: function(data) {
            
            $('#desqloqueaBox').html('');
            
            $.each(data, function(index, marca) {
                if (marca.id_m == id) {
                    $('#desqloqueaBox').append(boxEquipos + '<img src="img/' + marca.img + '">' + '<p class="is-hidden" val=' + marca.id + '>' + marca.name + '</p>' + end);
                }
            });

            $('html, body').animate({ scrollTop: 0 }, 'fast');

            //CARGA LA MARCA SELECCIONADA
            $('.equipoName').on('click',function(){

                equipo = $(this).find('p').attr('val');

                secondStep();

            })

        },
        error: function(xhr, tst, err) {
          console.log(err);
        }
    })

}

//CARGA PAISES Y OPERADORES SEGUNDO PASO ANTES DEL PAGO
function secondStep(){

    var url = 'desbloquear.php';
    var form = $('<form action="' + url + '" method="POST" class="is-hidden">' +
    '<input type="text" name="paso" value=2 />' +
    '<input type="text" name="id" value="' + id + '" />' +
    '<input type="text" name="equipo" value="' + equipo + '" />' +
    '<input type="text" name="marca" value="' + marca + '" />' +
    '</form>');
    $('body').append(form);
    form.submit();

};

//CARGA IMEI ULTIMO PASO ANTES DEL PAGO
$('.thirdStepOrder').on('click',function(){

    var id = $('#equipo').html();
    var country = $('#selectCountry').val();
    var operator = $('#selectOperator').val();

    var url = 'desbloquear.php';
    var form = $('<form action="' + url + '" method="POST" class="is-hidden">' +
    '<input type="text" name="paso" value=3 />' +
    '<input type="text" name="id" value="' + id + '" />' +
    '<input type="text" name="marca" value="' + marca + '" />' +
    '<input type="text" name="equipo" value="' + id + '" />' +
    '<input type="text" name="pais" value="' + country + '" />' +
    '<input type="text" name="operator" value="' + operator + '" />' +
    '</form>');
    $('body').append(form);
    form.submit();

});


//CARGA LA MARCA SELECCIONADA
$('.payConfirm').on('click',function(){

	pago = $(this).attr('val');

	marca = $('#marcaBrand').html();
	equipo = $('#equipoBrand').html();
    pais = $("#pais").html();
    operator = $("#operator").html();
    name = $("#name").val();
    imei = $("#imei").val();
	precio = $(".precioDesbloqueo").html();

	var text = "<img class='img-center pa-one' src='img/logo_500.png'>";
	text += "<h5 class='has-text-white has-text-weight-light is-marginless'>Your Name: <i class='has-text-white has-text-weight-bold is-marginless'>" + name + "</i></h5>";
	text += "<h5 class='has-text-white has-text-weight-light is-marginless'>Email: <i class='has-text-white has-text-weight-bold is-marginless'>" + email + "</i></h5>";
	text += "<h5 class='has-text-white has-text-weight-light is-marginless'>IMEI: <i class='has-text-white has-text-weight-bold is-marginless is-size-4'>" + imei + "</i></h5>";
    text += "<h5 class='has-text-white has-text-weight-light is-marginless'>Country: <i class='has-text-white has-text-weight-bold is-marginless'>" + pais + "</i></h5>";
    text += "<h5 class='has-text-white has-text-weight-light is-marginless'>Operator: <i class='has-text-white has-text-weight-bold is-marginless'>" + operator + "</i></h5>";
	text += "<h5 class='has-text-white has-text-weight-light is-marginless'>Equipment: <i class='has-text-white has-text-weight-bold is-marginless'>" + marca + ' - ' + equipo + "</i></h5>";
	text += "<h5 class='has-text-white has-text-right'>Cost: <b class='has-text-weight-semibold is-size-3'>$" + precio + ".00</b></h5>";
    text += "<small class='has-text-white'>Press 'ESC' to cancel your order</small";

	// set content
	modal.setContent(text);
    $('.tingle-modal-box__content').addClass('background-p-1');
	modal.open();

})

//CHECADORES DE INPUTS
//IMEI
$('#imei').on('keyup', function() {
    if (this.value.length < 15 || this.value.length > 15) {
    	$('.imeiCheck').removeClass('is-hidden');
    	$('.paymentCheck').addClass('is-hidden');
    } else {
    	$('.imeiCheck').addClass('is-hidden');
    	$('.paymentCheck').removeClass('is-hidden');
    }
    if (this.value.length == 0) {
    	$('.imeiCheck').addClass('is-hidden');
    	$('.paymentCheck').addClass('is-hidden');
    }
});

//EMAIL
$('#email').on('keyup', function() {
	email = $(this).val();
	if (validateEmail(email)) {
    	$('.emailCheck').addClass('is-hidden');
	} else {
    	$('.emailCheck').removeClass('is-hidden');
	}
    
    if (this.value.length == 0) {
    	$('.emailCheck').addClass('is-hidden');
    }
});

//CONFIMATION
$('#confirm').on('keyup', function() {
	email = $('#email').val();
	confirm = $(this).val();
	if (confirm != email) {
    	$('.confirmCheck').removeClass('is-hidden');
    	$('.paymentCheck').addClass('is-hidden');
	} else {
    	$('.confirmCheck').addClass('is-hidden');
    	$('.paymentCheck').removeClass('is-hidden');
	}
    
    if (this.value.length == 0) {
    	$('.confirmCheck').addClass('is-hidden');
    	$('.paymentCheck').addClass('is-hidden');
    }
});


function validateEmail(sEmail) {
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if (filter.test(sEmail)) {
		return true;
	}
	else {
		return false;
	}
}

//CARGA FOOTER
function cargaMarcasFooter(e) {

    $.ajax({
        type: "POST",
        url: "php/home/carga-marcas.php",
        dataType: "json",
        context: document.body,

        success: function(data) {

            $.each(data, function(index, home) {
                if (home.portada == 1) {
                    $('#menuFooter').append('<li class="marcaFooter handed" val=' + home.id + ' mar=' + home.name + '><a class="white-text">' + home.name + '</a></li>');
                }
            });

            //CARGA LA MARCA SELECCIONADA
            $('.marcaFooter').on('click',function(){

                var val = $(this).attr('val');
                var marca = $(this).attr('mar');

                //cargaEquipos(val, marca);
                cargaImei(val, marca);

            })
        },

        error: function(xhr, tst, err) {
            console.log(err);
        }

    });

};

//CARGA IMEI ULTIMO PASO ANTES DEL PAGO
function cargaImei(id, mar) {

    var id = id;
    var marca = mar;

    var url = 'desbloquear.php';
    var form = $('<form action="' + url + '" method="POST" class="is-hidden">' +
    '<input type="text" name="paso" value=1 />' +
    '<input type="text" name="id" value="' + id + '" />' +
    '<input type="text" name="marca" value="' + marca + '" />' +
    '</form>');
    $('body').append(form);
    form.submit();


}

//MENU DESBLOQUEOS
function cargaMenu(e) {

    //NUMERO DE COMENTARIOS
    $.ajax({
        url: 'php/home/carga-marcas.php',
        contentType: "application/json",
        context: document.body,
        success: function(data) {

            $('#menuTotal').html();

            $.each(data, function(index, home) {
                $('#desbloqueaMarcasMenu').append('<a class="navbar-item marcaNameMenu" val=' + home.id + ' mar=' + home.name + '>' + home.name + '</a>');
            });

            //CARGA LA MARCA SELECCIONADA
            $('.marcaNameMenu').on('click',function(){

                var val = $(this).attr('val');
                var marca = $(this).attr('mar');

                //cargaEquipos(val, marca);
                cargaImei(val, marca);

            })

        },

        error: function(xhr, tst, err) {
            console.log(err);
        }

    });

}