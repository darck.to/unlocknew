//SCRIPT DE PAYPAL PARA EL BOTON Y PAGOS
var imei = $("#imei").val();
var email = $('#email').val();
var name = $("#name").val();
var marca = $('#marcaBrand').html();
var equipo = $('#equipoBrand').html();
var precio = $("#precioDesbloqueo").html();

var order;

var form_data = new FormData();

form_data.append("imei", imei);
form_data.append("email", email);
form_data.append("name", name);
form_data.append("marca", marca);
form_data.append("equipo", equipo);
form_data.append("precio", precio);

paypal.Button.render({
env: 'production',
style: {
    shape: 'rect',
    size: 'responsive',
    tagline: 'false'
},
locale: 'en_US',
client: {
production: 'AfTNcysU0h84ZpFXozs32EOtbSkrMMzPbIDWYUVDo1LBQ5xPEwW4JMcMhsmzr0AF2d6N39xkLGa3K4Tf'
},
payment: function (data, actions) {
	
	return actions.payment.create({
	  transactions: [{
	    amount: {
	      total: '' + precio +'',
	      currency: 'USD'
	    },
	    description: 'Payment MovilSIM Services',
	    custom: '9004863002',
	    //invoice_number: '12345', Insert a unique invoice number
	  }]
	});

},

onAuthorize: function (data, actions) {
	
	return actions.payment.execute()
	  .then(function () {

	    cargaCompra();

	  });
	}

}, '#paypal-button');


//CARGA LA COMPRA PARA EL USUARIO
function cargaCompra(e) {

	//SOLICITAMOS EL CODIGO
	$.ajax({

	    method: 'post',
	    url: 'php/desbloquea/desbloquea-order.php',
	    data: form_data,
	    cache: false,
	    contentType: false,
	    processData: false,
	    dataType: "html",
	    context: document.body,
	    success: function(data) {

	        modal.close();
	        
	        $('#orderDiv').html('').delay(2000);
            $('#orderDiv').addClass('is-hidden');
            $('#orderDiv').addClass('border-r-10 orange lighten-3 ten-p');
            $('#orderDiv').append('<h4 class="center-align">Tu n&uacute;mero de orden es: <b>' + data + '</b></h4>');
            $('#orderDiv').append('<h5 class="center-align">Guardalo para futuras referencias</h5>');
            $('#orderDiv').removeClass('is-hidden');

	    },
	    error: function(xhr, tst, err) {
	        console.log(err);
	    }

	});

}