//INICIALIZAMOS
var imei;
var email;

// instanciate new modal
var modal = new tingle.modal({
    footer: true,
    stickyFooter: false,
    closeMethods: [],
    closeMethods: ['overlay', 'button', 'escape'],
    cssClass: ['custom-class-1', 'custom-class-2'],
    onOpen: function() {
    },
    onClose: function() {
    },
    beforeClose: function() {
        return true;
    }
});

modal.addFooterBtn('Confirm your Request', 'tingle-btn tingle-btn--primary', function() {
        
    //PROCESA PAGO
    var form_data = new FormData();

    form_data.append("imei", imei);
    form_data.append("email", email);

    //SOLICITAMOS EL CODIGO
    $.ajax({

        method: 'post',
        url: 'php/revisa/carga-imei.php',
        data: form_data,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "html",
        context: document.body,
        success: function(data) {

            modal.close();
            
            $('#orderDiv').addClass('fadeOut').delay(2000);
            $('#orderDiv').html('').delay(2000);
            $('#orderDiv').addClass('hide');
            $('#orderDiv').addClass('border-r-10 orange lighten-3 ten-p');
            $('#orderDiv').append('<h4 class="center-align">Tu n&uacute;mero de orden es: <b>' + data + '</b></h4>');
            $('#orderDiv').append('<h5 class="center-align">Guardalo para futuras referencias</h5>');
            $('#orderDiv').removeClass('hide');
            $('#orderDiv').removeClass('fadeOut');
            $('#orderDiv').addClass('fadeIn');

        },
        error: function(xhr, tst, err) {
            console.log(err);
        }

    });

});

$(document).ready(function(){


});

//CARGA LA MARCA SELECCIONADA
$('.button-carga-imei').on('click',function(){

	imei = $("#imeiForm").val();
	mail = $("#emailImeiForm").html();

	var text = "<img class='responsive-img' src='img/logo.png'>";
	text += "<div class='section'></div>";
	text += "<h5 class='grey-text text-darken-1 no-m'>Email: <h6 class='grey-text text-darken-2 no-m'>" + email + "</h6></h5>";
	text += "<h5 class='grey-text text-darken-1 no-m'>IMEI: <h6 class='grey-text text-darken-2 no-m'>" + imei + "</h6></h5>";
    text += "<div class='section'></div>";
    text += "<p class='grey-text text-darken-1 no-m'>We will contact you when the IMEI code is reviewed and we have an answer</p>";

	// set content
	modal.setContent(text);

	modal.open();

})

//CHECADORES DE INPUTS
//IMEI
$('#imeiForm').on('keyup', function() {
    if (this.value.length < 15 || this.value.length > 15) {
        $(this).css('background-color','rgba(255,0,0,0.3)');
        $('.imeiCheck').removeClass('hide');
        $('.paymentCheck').addClass('hide');
    } else {
        $(this).css('background-color','rgba(255,0,0,0)');
        $(this).css('border-bottom','1px solid #9e9e9e');
        $('.imeiCheck').addClass('hide');
        $('.paymentCheck').removeClass('hide');
    }

    if (this.value.length == 0) {
        $(this).css('background-color','rgba(255,0,0,0)');
        $(this).css('border-bottom','1px solid #9e9e9e');
        $('.imeiCheck').addClass('hide');
        $('.paymentCheck').addClass('hide');
    }
});

//EMAIL
$('#emailImeiForm').on('keyup', function() {
    email = $(this).val();
    if (validateEmail(email)) {
        $(this).css('background-color','rgba(255,0,0,0)');
        $(this).css('border-bottom','1px solid #9e9e9e');
        $('.emailCheck').addClass('hide');
    } else {
        $(this).css('background-color','rgba(255,0,0,0.3)');
        $('.emailCheck').removeClass('hide');
    }
    
    if (this.value.length == 0) {
        $(this).css('background-color','rgba(255,0,0,0)');
        $(this).css('border-bottom','1px solid #9e9e9e');
        $('.emailCheck').addClass('hide');
    }
});


function validateEmail(sEmail) {
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if (filter.test(sEmail)) {
		return true;
	}
	else {
		return false;
	}
}