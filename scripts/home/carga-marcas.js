//MARCAS
var box = '<div class="tile is-parent is-2 marca-encajonada handed"><div class="marcaName">';
var end = '</div></div>';

var marca;

$(document).ready(function(){
	
	cargaMarcas();
	cargaMenu();
	cargaMarcasFooter();

	$.fn.toggleText = function(t1, t2){
	  if (this.text() == t1) this.text(t2);
	  else                   this.text(t1);
	  return this;
	};

});

function cargaMarcas(e) {

	$.ajax({
		type: "POST",
		url: "php/home/carga-marcas.php",
		dataType: "json",
		context: document.body,

		success: function(data) {
			console.log(data);

			$.each(data, function(index, home) {
				if (home.portada == 1) {
	            	$('#marcaBox').append(box + '<img class="marca-brand" src="img/' + home.img + '">' + '<div><p val=' + home.id + ' mar=' + home.name + '>' + home.name + '</p></div>' + end);
				}
	        });

			$('#marcaBox').append('<div class="marcasHide tile is-parent is-12 is-paddingless wrapped"></div>');

	        $.each(data, function(index, home) {
				if (home.portada != 1) {
	            	$('.marcasHide').append(box + '<img class="marca-brand" src="img/' + home.img + '">' + '<div><p val=' + home.id + ' mar=' + home.name + '>' + home.name + '</p></div>' + end);
				}
	        });

	        $('#marcaBox').append("<div class=\"showMarcas has-text-weight-bold handed font-s-2\"><p>MOSTRAR M&Aacute;S</p><i class='icon-show fas fa-chevron-down fa-5x'></i></div>");

	        //CARGA LA MARCA SELECCIONADA
			$('.marcaName').on('click',function(){

				var val = $(this).find('p').attr('val');
				var marca = $(this).find('p').attr('mar');

				//cargaEquipos(val, marca);
				cargaImei(val, marca);

			});

			//OCULTA LAS MARCAS EXTRAS
			$('.marcasHide').addClass('is-sr-only');

			//MUESTRA LAS MARCAS OCULTAS
			$('.showMarcas').on( 'click' , function () {

				$('.marcasHide').toggleClass('is-sr-only');
				$('.icon-show').toggleClass('fa-chevron-down');
				$('.icon-show').toggleClass('fa-chevron-up');
				$('.showMarcas p').toggleText('OCULTAR', 'MOSTRAR MÁS');

			});

		},

		error: function(xhr, tst, err) {
			console.log(err);
		}

	});

};

function cargaEquipos(val, mar) {

	var idSelect = val;
	var idMarca = mar;

	$.ajax({
		type: "POST",
        url: "php/desbloquea/carga-equipos.php",
        dataType: "json",
		context: document.body,
        success: function(data) {
      		
        	$('#desqloqueaBox').html('');
        	$('#desqloqueaBox').removeClass('hidden');
        	$('#desqloqueaBox').addClass('fadeIn border-r yellow lighten-4 ten-p');
			
        	$.each(data, function(index, marca) {
        		if (marca.id_m == idSelect) {
	            	$('#desqloqueaBox').append(boxEquipos + '<img class="marca-brand" src="img/' + marca.img + '">' + '<div class="center-align"><p val=' + marca.id + '>' + marca.name + '</p></div>' + end);
        		}
	        });

        	$('#desqloqueaBox').append("<h6 class='right right-align grey-text text-lighten-1'><b>Selecciona tu equipo para continuar el proceso</b></h6>");

	        $('html, body').animate({ scrollTop: 0 }, 'fast');

	        //CARGA LA MARCA SELECCIONADA
			$('.equipoName').on('click',function(){

				var val = $(this).find('p').attr('val');

				cargaImei(val, idMarca);

			})

        },
        error: function(xhr, tst, err) {
          console.log(err);
        }
    })

}

//CARGA IMEI ULTIMO PASO ANTES DEL PAGO
function cargaImei(id, mar) {

	var id = id;
	var marca = mar;

	var url = 'desbloquear.php';
	var form = $('<form action="' + url + '" method="POST" class="hide">' +
	'<input type="text" name="paso" value=1 />' +
	'<input type="text" name="id" value="' + id + '" />' +
	'<input type="text" name="marca" value="' + marca + '" />' +
	'</form>');
	$('body').append(form);
	form.submit();


}


//OMNI Buscador
$("input.omniSearch").on("keyup", function(e) {
	
    var input, filter, parent, contenedor, p, i;

    input = $(this);
    
    filter = input.val().toUpperCase();

    parent = $('#marcaBox > .tile');

    contenedor = $('#marcaBox > .tile > .marcaName > div')
    
    for (i = 0; i < contenedor.length; i++) {
        
        p = contenedor[i].getElementsByTagName("p")[0];
        
        if (p.innerHTML.toUpperCase().indexOf(filter) > -1) {

            parent[i].style.display = "";

        } else {

            parent[i].style.display = "none";

        }

    }

    var j;

    parentHid = $('#marcaBox > .marcasHide > .tile');

    hideContenedor = $('#marcaBox > .marcasHide > .tile > .marcaName > div')
    
    for (j = 0; j < hideContenedor.length; j++) {
        
        p = hideContenedor[j].getElementsByTagName("p")[0];
        
        if (p.innerHTML.toUpperCase().indexOf(filter) > -1) {

        	$('.marcasHide').removeClass('is-sr-only');
            parentHid[j].style.display = "";

        } else {

        	$('.marcasHide').addClass('is-sr-only');
            parentHid[j].style.display = "none";

        }

    }


});

function cargaMenu(e) {

	//NUMERO DE COMENTARIOS
	$.ajax({
		url: 'php/home/carga-marcas.php',
		contentType: "application/json",
        context: document.body,
        success: function(data) {

        	$('#menuTotal').html();

        	$.each(data, function(index, home) {
	            $('#desbloqueaMarcasMenu').append('<a class="navbar-item marcaNameMenu" val=' + home.id + ' mar=' + home.name + '>' + home.name + '</a>');
	        });

	        //CARGA LA MARCA SELECCIONADA
			$('.marcaNameMenu').on('click',function(){

				var val = $(this).attr('val');
				var marca = $(this).attr('mar');

				//cargaEquipos(val, marca);
				cargaImei(val, marca);

			})

        },

		error: function(xhr, tst, err) {
			console.log(err);
		}

	});

}

function cargaMarcasFooter(e) {

	$.ajax({
		type: "POST",
		url: "php/home/carga-marcas.php",
		dataType: "json",
		context: document.body,

		success: function(data) {
			console.log(data);

			$.each(data, function(index, home) {
				if (home.portada == 1) {
	            	$('#menuFooter').append('<li class="marcaFooter handed" val=' + home.id + ' mar=' + home.name + '>' + home.name + '</li>');
				}
	        });

			//CARGA LA MARCA SELECCIONADA
			$('.marcaFooter').on('click',function(){

				var val = $(this).attr('val');
				var marca = $(this).attr('mar');

				//cargaEquipos(val, marca);
				cargaImei(val, marca);

			})
		},

		error: function(xhr, tst, err) {
			console.log(err);
		}

	});

};