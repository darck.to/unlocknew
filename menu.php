<nav class="navbar  background-p-1" role="navigation" aria-label="main navigation">
  <div class="container">
    <div class="navbar-brand">
      <a class="navbar-item has-text-white has-text-weight-bold logo-menu" href="index.php"></a>

      <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
        <span aria-hidden="true"></span>
      </a>
    </div>

    <div id="navbarBasicExample" class="navbar-menu">
      <div class="navbar-end">
        <div class="navbar-item has-dropdown is-hoverable">
          <a class="navbar-link navbar-item has-text-white has-text-weight-bold">
            <?php echo _("DESBLOQUEAR"); ?>
          </a>

          <div id="desbloqueaMarcasMenu" class="navbar-dropdown">
          </div>
        </div>

        <a class="navbar-item has-text-white has-text-weight-bold">
          <?php echo _("REVISAR IMEI"); ?>
        </a>

        <a class="navbar-item has-text-white has-text-weight-bold">
          <?php echo _("CUPONES"); ?>
        </a>

        <a class="navbar-item has-text-white has-text-weight-bold">
          <?php echo _("PREGUNTAS A EXPERTOS"); ?>
        </a>

        <a class="navbar-item has-text-white has-text-weight-bold">
          <?php echo _("CONTACTO"); ?>
        </a>

        <a class="navbar-item has-text-white has-text-weight-bold">
          <i class="fas fa-user"></i>
        </a>
      </div>
    </div>
  </div>
</nav>