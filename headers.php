<?php
	$language = "en_US";
	putenv("LANG=".$language);
	setlocale(LC_ALL, $language);

	$domain = "translation";
	bindtextdomain($domain, "locale");
	textdomain($domain);
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
	<link href="estilos/reset.css" type="text/css" rel="stylesheet">

	<!--Import bulma.css-->
	<link type="text/css" rel="stylesheet" href="css/bulma.css"  media="screen,projection"/>
	<!--FontAwesome-->
	<link type="text/css" rel="stylesheet" href="css/all.min.css"  media="screen,projection"/>
	<!--Import tingle.css-->
	<link type="text/css" rel="stylesheet" href="css/tingle.css"  media="screen,projection"/>

	<link href="estilos/estilos.css" type="text/css" rel="stylesheet">
	<link rel="icon" type="image/png" href="favicon.png">
	<!--[if lt IE 9]>
	    <script src="scripts/html5shiv.js"></script>
	<![endif]-->
	<title>iUnlock MX</title>
</head>