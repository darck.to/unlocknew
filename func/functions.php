<?php	

	//GENERADOR DE CADENAS ALEATORIAS
	function generateRandomString($length = 16) {
	    $characters = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	function leeFolio() {
		//NOMBRE DE ARCHIVO
		$filename = '../../admin/assets/folio.json';
		if (file_exists($filename)) {
			$filename = file_get_contents($filename);
			$data = json_decode($filename, true);
			$folio = $data[0]['folio'];
		}
		return $folio;
	}

	function masFolio() {
		$filename = file_get_contents('../../admin/assets/folio.json');
		$data = json_decode($filename, true);
		$viejo = $data[0]['folio'];
		$data[0]['folio'] = $viejo + 1;
		//LO VOLVEMOS A GUARDAR
		$newJsonString = json_encode($data);
		file_put_contents('../../admin/assets/folio.json', $newJsonString);
	}

	//LIMPIAMOS LAS VARIABLES
	function html_escape($html_escape) {
        $html_escape =  htmlspecialchars($html_escape, ENT_QUOTES | ENT_HTML5, 'UTF-8');
        return $html_escape;
    }

?>