<?php

	//BUSCAMOS LA INFORMACION DE PERFIL EN BASE AL LINKU
	function enviaCorreo($asunto,$cuerpo,$mail) {
		// Enviamos por email la nueva contraseña
		$remite_nombre = "UnlockMX Notifica";
		$remite_email = "no-reply@iunlockmx.com.mx";
		$mensaje = cuerpoMail($cuerpo);
		$cabeceras = "From: ".$remite_nombre." <".$remite_email.">\r\n";
		$cabeceras = $cabeceras."Mime-Version: 1.0\n";
		$cabeceras = $cabeceras."Content-Type: text/html";
		//CORREO DEL REMITENTE
		$usuario_email = $mail;
		$enviar_email = mail($usuario_email,$asunto,$mensaje,$cabeceras);

		if ($enviar_email) {
			echo "Mensaje Enviado! ";
		} else {
			echo "No se ha podido enviar el email";
		}
	}

	function cuerpoMail($cuerpo) {
		$logo = "
			<a style='color: #039be5; text-decoration: none; -webkit-tap-highlight-color: transparent;' href='https://iunlock.com.mx/'><img style='display: block; margin: 0 auto; height: 180px;' src='http://iunlock.com.mx/img/logo_index.png'/></a>
		";
		$bgInit = "
			<div style='position: relative; width: 100%; height: 900px; padding-top: 20px; background-color: #cf6e50;'>
		";
		$body = "
				<div style='position: relative; margin: 0 auto; width: 450px; height: 700px; z-index: 2;'>
		";
		$header = "
					<div style='position: relative; width: 100%; height: 200px; padding: 15px; background-color: #90a4ae; color: white; text-align: center; font-size: 1.5rem; font-weight: lighter;'>".$logo."</div>
		";
		$content = "
					<div style='position: relative; width: 100%; height: 550px; padding: 15px; background-color: #ededed; color: #3b3b3b; text-align: left; font-weight: lighter;'>".$cuerpo."</div>
		";
		$footer = "
					<div style='position: relative; width: 100%; height: 50px; padding: 15px; color: #3b3b3b; background-color: #d4d4d4;'>
						<a href='http://iunlock.com.mx' style='float: left; position: relative; color: #039be5; text-decoration: none; -webkit-tap-highlight-color: transparent; color: #9f3611; font-weight: bolder;'>iUnlockMX</a>
						<i style='float: right; position: relative'>Todos los Derechos Reservados 2018</i>
					</div>
		";
		$bodyClose = "
				</div>
		";
		$bgClose = "
			</div>
		";
		return $bgInit.$body.$header.$content.$footer.$bodyClose.$bgClose;
	}

?>