<!--BOX PROCESO-->
<div class="box is-radiusless is-shadowless background-p-1">
	<div class="columns">
		<div class="column pa-one">
			<div class="content pa-one background-p-3 radius-5">
				<h2 class="has-text-white"><i id="marcaBrand"></i><small>&reg;</small></h2>
				<h6 class="has-text-white"><?php echo _("Selecciona el modelo de tu equipo");?></h6>
				
				<div id="desqloqueaBox" class="tile is-ancestor wrapped pa-one"></div>

				<h6 class="has-text-white has-text-weight-light"><small><?php echo _("NOTA: De no existir modelos para tu marca, vuelve a seleccionar la marca de tu equipo"); ?></small></h6>

				<!--BARRA DE PROGRESO-->
				<div class="columns">
					<div class="column pa-one">
						<div class="content pa-one background-p-3 radius-5">
							<progress class="progress is-large is-warning" value="33" max="100">33%</progress>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>