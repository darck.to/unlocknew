<!--FULL WIDTH HEADER-->
<section class="hero is-warning hero-background">
	<div class="hero-body">
		<div class="container">
			<h1 class="title has-text-right has-text-weight-bold has-text-white font-four-rem">
			<?php echo _("LIBERA"); ?><p><?php echo _("TU TEL&Eacute;FONO"); ?></p>
			</h1>
		</div>
	</div>
</section>

<!--COMPLETA TU ORDEN-->
<div class="section">
	<div class="container">
		<h1 class="title has-text-centered has-text-weight-bold font-color-1"><?php echo _("COMPLETA TU ORDEN"); ?></h1>
		<h5 class="title has-text-centered is-size-6 font-s-1"><?php echo _("SELECCIONA TU MODELO, PA&Iacute;S, OPERADOR Y COMPLETA TUS DATOS"); ?></h5>
		
		<!--DATOS-->
		<?php

			if ($_POST['paso'] == 1) {

				//SECCION DE CUERPO
				echo "<div class='is-hidden' id='id'>".$_POST['id']."</div>";
				echo "<div class='is-hidden' id='marca'>".$_POST['marca']."</div>";

				include_once('templates/desbloquea/desbloquea-equipos.php');

			} elseif ($_POST['paso'] == 2) {

				//SECCION DE CUERPO
				echo "<div class='is-hidden' id='id'>".$_POST['id']."</div>";
				echo "<div class='is-hidden' id='marca'>".$_POST['marca']."</div>";
				echo "<div class='is-hidden' id='equipo'>".$_POST['equipo']."</div>";

				include_once('templates/desbloquea/desbloquea-selects.php');

			} elseif ($_POST['paso'] == 3) {

				//SECCION DE CUERPO
				echo "<div class='is-hidden' id='id'>".$_POST['id']."</div>";
				echo "<div class='is-hidden' id='marca'>".$_POST['marca']."</div>";
				echo "<div class='is-hidden' id='equipo'>".$_POST['equipo']."</div>";
				echo "<div class='is-hidden' id='pais'>".$_POST['pais']."</div>";
				echo "<div class='is-hidden' id='operator'>".$_POST['operator']."</div>";
				
				include_once('templates/desbloquea/desbloquea-imei.php');
			}

		?>

	</div>
</div>

<!--CONOCE TUS DATOS-->
<div class="section background-s-2">
	<div class="container">
		<h1 class="title has-text-centered has-text-weight-bold font-color-1"><?php echo _("CONOCE TUS DATOS"); ?></h1>
		<h5 class="title has-text-centered is-size-6 font-s-1"><?php echo _("SIGUE ESTOS PASOS PARA CONOCER TU IMEI Y LA FORMA DE DESBLOQUEAR TU TEL&Eacute;FONO"); ?></h5>
		<div class="box">
			<div class="columns">
				<div class="column is-8">
					<div class="content">
						<p>
							<h5><?php echo _("1. CONOCE EL IMEI DE TU EQUIPO:"); ?></h5>
							<ul>
								<li><?php echo _("MARCA "); ?><b>*#06#</b><?php echo _("&nbsp;PARA VER EL N&Uacute;MERO IMEI EN TU PANTALLA."); ?></li>
								<li><?php echo _("EL IMEI ES UN N&Uacute;MERO &Uacute;NICO E INTRANSFERIBLE QUE SE UTILIZA PARA REPORTAR ROBOS O BLOQUEAR DISPOSITIVOS."); ?></li>
							</ul>
							<h5><?php echo _("2. UNE VES QUE TENGAS TU C&Oacute;DIGO DE DESBLOQUEO:"); ?></h5>
							<ul>
								<li>A)&nbsp;<?php echo _("CON UNA ");?><b>SIM CARD</b><?php echo _("&nbsp;NO ACEPTADA POR EL EQUIPO, ENCIENDELO Y ESPERA A QUE SE INICIE");?></li>
								<li>B)&nbsp;<?php echo _("EL EQUIPO INDICAR&Aacute;: PIN DE DESBLOQUEO DE TARJETA SIM.");?></li>
								<li>C)&nbsp;<?php echo _("INTRODUCE EL C&Oacute;DIGO QUE TE PROPORCIONAMOS Y PULSA DESBLOQUEAR.");?></li>
							</ul>
							<h5><?php echo _("3. DISFRUTA TU EQUIPO "); ?><b class="font-color-2"><?php echo _("LIBERADO"); ?></b></h5>
						</p>
					</div>
				</div>
				<div class="column is-4">
					<div class="image">
						<img src="img/desbloquear-img.png" alt="Conoce tu IMEI">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--COMO DESBLOQUEAR UN-->
<div class="section is-hidden sectionInstrucciones">
	<div class="container">
		<h1 class="title has-text-centered has-text-weight-bold font-color-1"><?php echo _("COMO DESBLOQUEAR UN"); ?>&nbsp;<i id="sectionMarcaEquipo" class="text-upp"></i></h1>
		<div class="box">
			<div class="columns">
				<div class="column is-9">
					<div id="descPhone" class="content">
						
					</div>
				</div>
				<div class="column is-3">
					<div class="image">
						<img id="sectionImgEquipo" src="img/desbloquear-img.png" alt="Instrucciones de desbloqueo">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!--POR QUE DESBLOQUEAR CON NOSOTROS-->
<div class="section background-s-2">
	<div class="container">
		<h1 class="title has-text-centered has-text-weight-bold font-color-1"><?php echo _("¿POR QUE DESBLOQUEAR CON NOSOTROS?"); ?></h1>
		<h5 class="title has-text-centered is-size-6 font-s-1"><?php echo _("CONOCE LAS RAZONES QUE NOS DISTINGUEN DE NUESTRA COMPETENCIA"); ?></h5>
		<nav class="level">
			<div class="level-item has-text-centered">
				<div class="boxed-shadow">
					<i class="fas fa-5x fa-check-circle font-color-2 pa-b-one"></i>
					<p class="heading is-size-4"><?php echo _("PERMANENTE");?></p>
					<p class="title is-size-5 has-text-weight-light"><?php echo _("El desbloqueo con nuestro");?></p>
					<p class="title is-size-5 has-text-weight-light"><?php echo _("sistema se mantiene por toda");?></p>
					<p class="title is-size-5 has-text-weight-light"><?php echo _("la vida &uacute;til de tu dispositivo.");?></p>
				</div>
			</div>
			<div class="level-item has-text-centered">
				<div class="boxed-shadow">
					<i class="fas fa-5x fa-thumbs-up font-color-2 pa-b-one"></i>
					<p class="heading is-size-4"><?php echo _("GARANTIZADO");?></p>
					<p class="title is-size-5 has-text-weight-light"><?php echo _("Te garantizamos atenci&oacute;n");?></p>
					<p class="title is-size-5 has-text-weight-light"><?php echo _("de primera y calidad en nuestro");?></p>
					<p class="title is-size-5 has-text-weight-light"><?php echo _("servicio y productos.");?></p>
				</div>
			</div>
			<div class="level-item has-text-centered">
				<div class="boxed-shadow">
					<i class="fas fa-5x fa-question-circle font-color-2 pa-b-one"></i>
					<p class="heading is-size-4"><?php echo _("SOPORTE 24/7");?></p>
					<p class="title is-size-5 has-text-weight-light"><?php echo _("¿Tienes una duda?");?></p>
					<p class="title is-size-5 has-text-weight-light"><?php echo _("Cont&aacute;ctanos, nosotros podemos");?>
					<p class="title is-size-5 has-text-weight-light"><?php echo _("ayudar a resolver tu duda.");?></p>
				</div>
			</div>
		</nav>
	</div>
</div>
