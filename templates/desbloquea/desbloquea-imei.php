<!--BOX PROCESO-->
<div class="box is-radiusless is-shadowless background-p-1">
	<div class="columns">
		<div class="column is-3 pa-one">
			<div class="image has-background-white pa-one shadow-blue">
				<img class="marca-brand" src="">
			</div>
		</div>
		<div class="column is-9 pa-one">
			<div class="content pa-one background-p-3 radius-5">
				<h4 class="has-text-white"><i id="marcaBrand"></i>&reg;&nbsp;|&nbsp;<b id="equipoBrand" class="text-upp"></b></h4>

				<div class="columns">
					<div class="column">
						<div class="content">
							<input class="input" id="name" type="text" name="name" placeholder="<?php echo _("Ingresa tus nombre"); ?>">
						</div>
					</div>
					<div class="column">
						<div class="content">
							<input class="input" id="email" type="email" name="email" class="validate" placeholder="<?php echo _("E-mail"); ?>">
							<small class="is-hidden red-text emailCheck"><?php echo _("El email debe ser valido"); ?></small>							
						</div>
					</div>
				</div>

				<div class="columns">
					<div class="column">
						<div class="content"></div>
					</div>
					<div class="column">
						<div class="content">
							<input class="input" id="confirm" type="email" name="confirm" class="validate" placeholder="<?php echo _("Confirma tu e-mail"); ?>">
							<small class="is-hidden confirmCheck"><?php echo _("El email debe ser igual"); ?></small>		
						</div>
					</div>
				</div>

				<div class="control ma-b-one">
					<input class="input" id="imei" type="text" name="imei" placeholder="<?php echo _("Ingresa el IMEI de tu equipo"); ?>">
					<small class="is-hidden imeiCheck"><?php echo _("El IMEI debe ser de 15 d&iacute;gitos"); ?></small>
				</div>

				<div class="is-hidden ma-b-one paymentCheck">
					<div class="content pa-one">
						<h5 class="has-text-centered has-text-weight-semibold has-text-white"><?php echo _("FELICIDADES, ES POSIBLE DESBLOQUEAR TU EQUIPO");?></h5>
						<h5 class="has-text-centered has-text-weight-light has-text-white is-paddingless"><?php echo _("Selecciona tu m&eacute;todo de desbloqueo y la forma de pago"); ?></h5>
					</div>
					
					<!--CAJA DE OFERTAS-->
					<div class="columns is-multiline">
						<div class="column is-12 has-background-white ma-one width-one__tworem">
							<div class="tile is-ancestor">
								<div class="tile is-parent is-3">
									<div class="content">
										<h2 class="has-text-weight-bold has-text-centered precioDesbloqueo">99.99</h2>
									</div>
								</div>
								<div class="tile is-parent is-4">
									<div class="content">
										<p class="is-marginless is-capitalized is-size-6 has-text-weight-semibold has-text-justified font-s-1">
											<?php echo _("Servicio Express");?>
										<br><?php echo _("12 A 24 horas");?>
										<br><?php echo _("DESBLOQUEO POR IMEI");?>
										</p>
									</div>
								</div>
								<div class="tile is-parent is-5 background-p-2 handed">
									<div class="content">
										<p class="has-text-centered has-text-weight-bold is-size-5 has-text-white"><?php echo _("COMPRAR");?></p>
									</div>
								</div>
							</div>
						</div>
						<div class="column is-12 has-background-white ma-one width-one__tworem">
							<div class="tile is-ancestor">
								<div class="tile is-parent is-3">
									<div class="content">
										<h2 class="has-text-weight-bold has-text-centered precioDesbloqueo">99.99</h2>
									</div>
								</div>
								<div class="tile is-parent is-4">
									<div class="content">
										<p class="is-marginless is-capitalized is-size-6 has-text-weight-semibold has-text-justified font-s-1">
											<?php echo _("Servicio Regular");?>
										<br><?php echo _("48 A 72 horas");?>
										<br><?php echo _("DESBLOQUEO POR IMEI");?>
										</p>
									</div>
								</div>
								<div class="tile is-parent is-5 background-p-2 handed">
									<div class="content">
										<p class="has-text-centered has-text-weight-bold is-size-5 has-text-white"><?php echo _("COMPRAR");?></p>
									</div>
								</div>
							</div>
						</div>
						<div class="column is-12 has-background-white ma-one width-one__tworem">
							<div class="tile is-ancestor">
								<div class="tile is-parent is-3">
									<div class="content">
										<h2 class="has-text-weight-bold has-text-centered precioDesbloqueo">99.99</h2>
									</div>
								</div>
								<div class="tile is-parent is-4">
									<div class="content">
										<p class="is-marginless is-capitalized is-size-6 has-text-weight-semibold has-text-justified font-s-1">
											<?php echo _("Servicio Regular");?>
										<br><?php echo _("48 A 72 horas");?>
										<br><?php echo _("DESBLOQUEO POR IMEI");?>
										</p>
									</div>
								</div>
								<div class="tile is-parent is-5 background-p-2 handed">
									<div class="content">
										<p class="has-text-centered has-text-weight-bold is-size-5 has-text-white"><?php echo _("COMPRAR");?></p>
									</div>
								</div>
							</div>
						</div>
					</div>

					<h5>Precio: <b>$250.00</b></h5>
					<i class="fab fa-paypal fa-3x handed payConfirm" val="0"></i>
					<p><?php echo _("Paypal"); ?></p>

					<i class="fas fa-hand-holding-usd fa-3x handed payConfirm" val="1"></i>
					<p><?php echo _("Efectivo"); ?></p>
				</div>

				<!--BARRA DE PROGRESO-->
				<div class="columns">
					<div class="column pa-one">
						<div class="content pa-one background-p-3 radius-5">
							<progress class="progress is-large is-warning" value="99" max="100">99%</progress>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!--ORDERBOX-->
	<div id="orderDiv"></div>

</div>