<!--BOX PROCESO-->
<div class="box is-radiusless is-shadowless background-p-1">
	<div class="columns">
		<div class="column is-3 pa-one">
			<div class="image has-background-white pa-one shadow-blue">
				<img class="marca-brand" src="">
			</div>
		</div>
		<div class="column is-9 pa-one">
			<div class="content pa-one background-p-3 radius-5">
				<h4 class="has-text-white"><i id="marcaBrand"></i>&reg;&nbsp;|&nbsp;<b id="equipoBrand"></b></h4>
				<h4 class="has-text-white"><?php echo _("Selecciona el país y operador de origen");?></h4>
				<h6><smal class="has-text-white has-text-weight-light"><?php echo _("No el operador al que quieras migrarlo");?></small></h6>
				
				<div class="columns">
					<div class="column">
						<div class="content">
							<div class="select is-fullwidth">
								<select id="selectCountry">
									<option value="" disabled selected><?php echo _("Escoge tu País"); ?></option>
								</select>
							</div>
						</div>
					</div>
					<div class="column">
						<div class="content">
							<div class="select is-fullwidth">
								<select id="selectOperator">
									<option value="" disabled selected><?php echo _("Escoge tu Operador"); ?></option>
								</select>
							</div>
						</div>
					</div>
				</div>

				<a class="button thirdStepOrder ma-b-one"><b><?php echo _("Siguiente"); ?></b></a>
				
				<!--BARRA DE PROGRESO-->
				<div class="columns">
					<div class="column pa-one">
						<div class="content pa-one background-p-3 radius-5">
							<progress class="progress is-large is-warning" value="66" max="100">66%</progress>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>