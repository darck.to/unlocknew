<!--FULL WIDTH HEADER-->
<section class="hero is-medium is-warning hero-background">
	<div class="hero-body">
		<div class="container">
			<h1 class="title has-text-right has-text-weight-bold has-text-white font-four-rem">
			<?php echo _("LIBERA"); ?><p><?php echo _("TU TEL&Eacute;FONO"); ?></p>
			</h1>
			<h2 class="subtitle has-text-right has-text-white font-two-rem">
			<?php echo _("r&aacute;pida y permenentemente"); ?>
			</h2>
		</div>
	</div>
</section>

<!--OMNI SEARCH-->
<div class="container">
	<div class="columns">
		<div class="column is-three-fifths is-offset-one-fifth">
			<div class="omni-buscador background-p-2">
				<input class="input is-relative omni-input omniSearch" type="text" placeholder="<?php echo _("BUSCA TU MODELO"); ?>">
			</div>
		</div>
	</div>
</div>

<!--MARCAS-->
<div class="section">
	<div class="container">
		<h1 class="title has-text-centered has-text-weight-bold font-color-1"><?php echo _("MARCAS"); ?></h1>
		<h5 class="title has-text-centered is-size-6 font-s-1"><?php echo _("SELECCIONA LA MARCA DE TU EQUIPO"); ?></h5>
		<div id="marcaBox" class="tile is-ancestor wrapped">
		</div>
	</div>
</div>

<!--NUESTROS PRINCIPIOS-->
<div class="section background-s-2">
	<div class="container">
		<h1 class="title has-text-centered has-text-weight-bold font-color-1"><?php echo _("NUESTROS PRINCIPIOS"); ?></h1>
		<nav class="level">
			<div class="level-item has-text-centered">
				<div>
					<div class="circle-icon has-text-centered">
						<i class="fas fa-thumbs-up font-color-2"></i>	
					</div>
					<p class="heading is-size-4"><?php echo _("GARANT&Iacute;A");?></p>
					<p class="title is-size-5 has-text-weight-light"><?php echo _("No corras riesgos,");?></p>
					<p class="title is-size-5 has-text-weight-light"><?php echo _("con nosotros el servicio");?></p>
					<p class="title is-size-5 has-text-weight-light"><?php echo _("es garantizado");?></p>
				</div>
			</div>
			<div class="level-item has-text-centered">
				<div>
					<div class="circle-icon has-text-centered">
						<i class="fas fa-fast-forward font-color-2"></i>
					</div>
					<p class="heading is-size-4"><?php echo _("VELOCIDAD");?></p>
					<p class="title is-size-5 has-text-weight-light"><?php echo _("Recibe tu c&oacute;digo");?></p>
					<p class="title is-size-5 has-text-weight-light"><?php echo _("en cuanto tu pago");?></p>
					<p class="title is-size-5 has-text-weight-light"><?php echo _("sea procesado");?></p>
				</div>
			</div>
			<div class="level-item has-text-centered">
				<div>
					<div class="circle-icon has-text-centered">
						<i class="fas fa-money-bill-wave font-color-2"></i>
					</div>
					<p class="heading is-size-4"><?php echo _("ECONOM&Iacute;A");?></p>
					<p class="title is-size-5 has-text-weight-light"><?php echo _("Con nuestros precios");?></p>
					<p class="title is-size-5 has-text-weight-light"><?php echo _("inigualables obtienes");?>
					<p class="title is-size-5 has-text-weight-light"><?php echo _("calidad y servicio");?></p>
				</div>
			</div>
			<div class="level-item has-text-centered">
				<div>
					<div class="circle-icon has-text-centered">
						<i class="fas fa-question font-color-2"></i>
					</div>
					<p class="heading is-size-4"><?php echo _("SOPORTE");?></p>
					<p class="title is-size-5 has-text-weight-light"><?php echo _("Contamos con chat");?></p>
					<p class="title is-size-5 has-text-weight-light"><?php echo _("y soporte 24/7");?></p>
					<p class="title is-size-5 has-text-weight-light"><?php echo _("cont&aacute;ctanos!");?></p>
				</div>
			</div>
		</nav>
	</div>
</div>

<!--COMENTARIOS DE NUESTROS CLIENTES-->
<div class="section">
	<div class="container">
		<h1 class="title has-text-centered has-text-weight-bold font-color-1"><?php echo _("COMENTARIOS DE NUESTROS CLIENTES");?></h1>
		<div class="columns">
			<div class="column is-6">
				<div class="box">
					<article class="media">
						<div class="media-content">
							<div class="content">
								<p>
									<strong id="commentsName" class="is-size-5 has-text-weight-bold"></strong> <small id="commentsPhone" class="is-size-5 as-text-weight-bold font-s-2"></small><br><small id="commentsDate" class="is-size-7 font-color-1"></small>
									<div class="columns">
										<div class="column is-1">
											<strong class="is-size-1 has-text-weight-bold font-color-2">"</strong>		
										</div>
										<div class="column">
											<p id="commentsDesc" class="font-s-1"></p>
										</div>
									</div>
								</p>
							</div>
						</div>
					</article>
				</div>
			</div>
			<div class="column is-6">
				<div class="box">
					<article class="media">
						<div class="media-content">
							<div class="content">
								<p>
									<strong id="commentsName2" class="is-size-5 has-text-weight-bold"></strong> <small id="commentsPhone2" class="is-size-5 as-text-weight-bold font-s-2"></small><br><small id="commentsDate2" class="is-size-7 font-color-1"></small>
									<div class="columns">
										<div class="column is-1">
											<strong class="is-size-1 has-text-weight-bold font-color-2">"</strong>		
										</div>
										<div class="column">
											<p id="commentsDesc2" class="font-s-1"></p>
										</div>
									</div>
								</p>
							</div>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</div>

<!--GRAY BLOCK-->
<div class="section background-s-1">
	<div class="container">
		<div class="columns">
			<div class="column is-three-fifths">
				<div class="box is-radiusless is-shadowless background-none">
					<div class="content">
						<h1 class="title has-text-left has-text-weight-bold has-text-white"><?php echo _("¿POR QUE LIBERAR TU EQUIPO CON NUESTRO SERVICIO?");?></h1>
						<p class="has-text-white"><?php echo _("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."); ?></p>
						<p class="has-text-white"><?php echo _("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."); ?></p>
					</div>
				</div>
			</div>
			<div class="column is-two-fifths logo-descripcion"><div class="logo-descripcion-p"></div></div>
		</div>
	</div>
</div>

<!--JS-->
<script type="text/javascript" src="scripts/home/carga-marcas.js"></script>